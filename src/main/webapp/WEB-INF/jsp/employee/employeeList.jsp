<%--
  Created by IntelliJ IDEA.
  User: Kboss
  Date: 2021/4/15
  Time: 13:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>Title</title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="${pageContext.request.contextPath}/css/emp/bootstrap.css" rel="stylesheet" type="text/css" media="all" >

    <!--//booststrap end-->

    <!-- font-awesome icons -->
    <link href="${pageContext.request.contextPath}/css/emp/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!--stylesheets-->
    <link href="${pageContext.request.contextPath}/css/emp/style.css" rel='stylesheet' type='text/css' media="all">

    <link href="${pageContext.request.contextPath}/css/emp/circles.css" rel="stylesheet" type="text/css" media="all" > <!--skill-circles-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/emp/main.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.2.1.min.js"></script>
   <%-- <!--//style sheet end here-->
    <link href="http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">--%>
</head>
<body>

<div class="agileits_main" style="background: url('${pageContext.request.contextPath}/images/b1.jpg')">
    <div class="banner" id="home">
        <!-- menu -->
        <div id="toggle_m_nav">
            <div id="m_nav_menu" class="m_nav">
                <div class="m_nav_ham" id="m_ham_1"></div>
                <div class="m_nav_ham" id="m_ham_2"></div>
                <div class="m_nav_ham" id="m_ham_3"></div>
            </div>
        </div>

        <div id="m_nav_container" class="m_nav">
            <ul id="m_nav_list" class="m_nav">
                <li class="m_nav_item" id="m_nav_item_1"> <a href="#home">顶部</a> </li>
                <li class="m_nav_item" id="moble_nav_item_2"> <a href="#about">个人信息</a> </li>
                <li class="m_nav_item" id="moble_nav_item_3"> <a href="#skills">爱好</a> </li>
                <li class="m_nav_item" id="moble_nav_item_4"> <a href="#portfolio">生活展示</a> </li>
                <li class="m_nav_item" id="moble_nav_item_5"> <a href="#education">信息修改</a> </li>
                <li class="m_nav_item" id="moble_nav_item_6"> <a href="#contact">安全</a> </li>
            </ul>
        </div>
        <!-- menu -->
        <div class="container">
            <div class="agile-logo">
                <h1><a href="index.html">个人信息</a></h1>
            </div>
        </div>
    </div>
    <!-- banner -->
    <div class="w3_banner" >
        <div class="container">
            <div class="slider">
                <div class="callbacks_container">
                    <ul class="rslides callbacks callbacks1" id="slider4">
                        <li>
                            <div class="banner_text_w3layouts">
                                <h3>世际酒店</h3>
                                <span> </span>
                                <p>就职于世际酒店</p>
                            </div>
                        </li>
                        <li>
                            <div class="banner_text_w3layouts">
                                <h3>兴趣爱好</h3>
                                <span> </span>
                                <p>我喜欢游泳</p>
                            </div>
                        </li>
                        <li>
                            <div class="banner_text_w3layouts">
                                <h3>工作认真</h3>
                                <span> </span>
                                <p>我的技能满分 </p>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <!-- //banner -->
</div>
<!-- //header -->
<!-- about -->
<div class="about" id="about">
    <div class="container">
        <div class="w3ls-titlew3l">
            <h3 style="color: white"></h3>
        </div>
        <div class="about-agileinfo">
            <div class="col-md-4 col-sm-4 about-left ">
                <img src="${pageContext.request.contextPath}/images/bio.jpg" class="img-responsive" alt=""/>
            </div>
            <div class="col-md-8 col-sm-8 about-right wthree">
                <h2>见到你很高兴  <span style="color: #8a6d3b">${emp.employeeName}</span></h2>
                <h4>工作要努力呀</h4>
                <ul class="address">
                    <li>
                        <ul class="agileits-address-text ">
                            <li><b>生日:</b></li>
                            <li>${emp.employeeBirthday}</li>
                        </ul>
                    </li>
                    <li>
                        <ul class="agileits-address-text">
                            <li><b>手机号 :</b></li>
                            <li>${emp.employeePhone}</li>
                        </ul>
                    </li>
                    <li>
                        <ul class="agileits-address-text">
                            <li><b>地址 :</b></li>
                            <li>${emp.employeeAdr}</li>
                        </ul>
                    </li>
                    <li>
                        <ul class="agileits-address-text">
                            <li><b>邮箱 :</b></li>
                            <li> ${emp.employeeEmail}</li>
                        </ul>
                    </li>
                    <li>
                        <ul class="agileits-address-text">
                            <li><b>员工账号 :</b></li>
                            <li> ${emp.employeeNumber}</li>
                        </ul>
                    </li>
                </ul>
                <div class="hire-w3lgrids">
                    <a href="#education" class="wthree-more nina" data-text="信息不对？">
                        <span>修</span><span>改</span><span>信</span><span>息</span>
                    </a>
                    <a href="#contact" class="wthree-more w3more1 nina" data-text="想看密码？">
                        <span>安</span><span>全</span><span>改</span><span>密</span>
                    </a>
                </div>
            </div>
            <div class="clearfix"> </div>

        </div>
    </div>
</div>
<!-- //about -->
<!-- skills -->
<div class="about-skills" id="skills">
    <div class="container">
        <div class="w3ls-titlew3">
            <h3>爱好-技能</h3>
        </div>

        <div class="skill-agileinfo">

            <div class="skill-grids">
                <div class="col-md-4 col-sm-4 skills-w3lsleft">
                    <h4>爱好</h4>
                    <ul>
                        <li><span class="fa fa-check-square-o" aria-hidden="true"></span>听歌</li>
                        <li><span class="fa fa-check-square-o" aria-hidden="true"></span>跑步</li>
                        <li><span class="fa fa-check-square-o" aria-hidden="true"></span>美食</li>
                        <li><span class="fa fa-check-square-o" aria-hidden="true"></span>游泳</li>
                        <li><span class="fa fa-check-square-o" aria-hidden="true"></span>游戏</li>
                        <li><span class="fa fa-check-square-o" aria-hidden="true"></span>阅读</li>

                    </ul>
                </div>
                <div class="col-md-8 col-sm-8 skills-w3lsright">
                    <h4>技能展示</h4>
                    <div class="skillbar" data-percent="98">
                        <span class="skillbar-title" style="background:#e08808;">厨艺</span>
                        <p class="skillbar-bar" style="background: #ff9d0d;"></p>
                        <span class="skill-bar-percent"></span>
                    </div>
                    <!-- End Skill Bar -->
                    <div class="skillbar" data-percent="99">
                        <span class="skillbar-title" style="background:#0396d8;">服务</span>
                        <p class="skillbar-bar" style="background:#03A9F4;"></p>
                        <span class="skill-bar-percent"></span>
                    </div>
                    <!-- End Skill Bar -->
                    <div class="skillbar" data-percent="95">
                        <span class="skillbar-title" style="background:#9226a4;">品鉴</span>
                        <p class="skillbar-bar" style="background:#b32eca;"></p>
                        <span class="skill-bar-percent"></span>
                    </div>
                    <!-- End Skill Bar -->
                    <div class="skillbar" data-percent="70">
                        <span class="skillbar-title" style="background:#028679;">微笑</span>
                        <p class="skillbar-bar" style="background: #009688;"></p>
                        <span class="skill-bar-percent"></span>
                    </div>
                    <!-- End Skill Bar -->
                </div>
                <div class="clearfix"></div>
            </div>


        </div>
    </div>
</div>
<!-- //skills -->
<!--start-->

<!--//start-->

<!-- gallery -->
<div class="gallery" id="portfolio">
    <div class="container">
        <div class="tittle-agileinfo">
            <h3>我的生活</h3>
        </div>
        <div class="w3ls_banner_bottom_grids ">
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#designs" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">工作</a></li>
                    <li role="presentation"><a href="#Alterations" role="tab" id="Alterations-tab" data-toggle="tab" aria-controls="Alterations">生活</a></li>
                    <li role="presentation"><a href="#Mending"  role="tab" id="Mending-tab" data-toggle="tab" aria-controls="Mending">旅游</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="designs" aria-labelledby="home-tab">
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-1.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-1.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-2.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-2.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-3.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-3.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-4.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-4.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-5.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-5.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-6.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-6.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-7.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-7.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/fashion-8.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/fashion-8.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Fashion</h4>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Alterations" aria-labelledby="Alterations-tab">
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/event-1.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/event-1.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Events</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/event-2.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/event-2.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Events</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/event-3.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/event-3.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Events</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/event-1.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/event-1.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Events</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="Mending" aria-labelledby="Mending-tab">
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/model-1.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/model-1.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Model</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/model-2.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/model-2.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Model</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/model-3.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/model-3.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Model</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/model-1.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/model-1.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Model</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/model-5.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/model-5.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Model</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 w3layouts_gallery_grid">
                            <a title="Portfolio" href="${pageContext.request.contextPath}/images/model-6.jpg">
                                <div class="w3layouts_team_grid">
                                    <img src="${pageContext.request.contextPath}/images/model-6.jpg" alt=" " data-selector="img" class="img-responsive" />
                                    <div class="w3layouts_team_grid_pos">
                                        <div class="wthree_text">
                                            <h4>Model</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //gallery -->


<!-- education -->
<div class="about-education" id="education">
    <div class="tittle-agileinfo">
        <h3>信息修改</h3>
    </div>
    <div class="container">

        <div class="container" style="background-color: #00a78e">


            <div class="booking-form-aits">

                <form>
                    <input type="text" value="${emp.employeeId}" style="display: none" id="eId">
                    <input id="eName" type="text" class="margin-right" Name="Name"  value="${emp.employeeName}">
                    <input id="ePhone" type="text"  Name="Phone Number" value="${emp.employeePhone}" required="">
                    <input id="eAdr" class="margin-right" type="text" value="${emp.employeeAdr}">
                    <div class="send-button agileits w3layouts">
                        <br>
                        <br>
                    </div>
                    <input id="eBirthday" type="date" class="margin-right"  required="">
                    <input name="uSex"  type="radio" value="男" <c:if test="${emp.employeeSex=='男'}"> checked="checked" </c:if>>男
                    <input name="uSex"  type="radio" value="女" <c:if test="${emp.employeeSex=='女'}"> checked="checked" </c:if>>女
                    <div class="send-button agileits w3layouts">
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="send-button agileits w3layouts">
                        <button type="button" onclick="updateOne()" class="btn btn-primary">修改</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- //education -->


<!-- Booking -->
<div class="w3layoutsbooking" id="contact" style="background: url('${pageContext.request.contextPath}/images/ba.jpg')">
    <div class="container" style="height: 600px">

        <h3>安全</h3>

        <div class="booking-form-aits" style="height: 500px">
            <div class="tittle-agileinfo">
                <span id="repeat" style="color: red"></span>
            </div>
            <form>

                <input id="eEmail" type="email" class="margin-right" Name="Email" value="${emp.employeeEmail}" required="">
                <div class="send-button agileits w3layouts">
                    <button type="button" onclick="sendEmailOne()" class="btn btn-primary">发送验证码更改账号密码</button>
                </div>
                <div class="send-button agileits w3layouts">
                    <br><span id="sssp" style="color: red"></span>
                    <br>
                </div>
                <input id="code" type="text" class="margin-right" Name="Phone Number" placeholder="在此输入验证码" required="">
                <div class="send-button agileits w3layouts">
                    <button type="button" onclick="yz()" class="btn btn-primary">验证</button>
                </div>
                <div class="send-button agileits w3layouts">
                    <br>
                    <br>
                </div>
                <input style="display: none" id="eNumber" type="text" class="margin-right" value="${emp.employeeNumber}" onchange="selecteOne()" required="">
                <input style="display: none" id="ePwd" type="text" class="margin-right" Name="Phone Number" placeholder="在此输入密码" required="">
                <input style="display: none" id="ePwdS" type="text" Name="Place" placeholder="请再输入一次密码" required="">
                <div class="send-button agileits w3layouts">
                    <br>
                    <br>
                </div>
                <div class="send-button agileits w3layouts">
                    <button id="bu" style="display: none" type="button" onclick="updateTwo()" class="btn btn-primary">修改账号密码</button>
                </div>
            </form>
        </div>

    </div>
</div>
<!-- //Booking -->


<!-- Footer -->

        <!-- Copyright -->

        <!-- //Copyright -->

    </div>
</div>
<!-- //Footer -->




<script type='text/javascript' src='${pageContext.request.contextPath}/js/emp/jquery-2.2.3.min.js'></script>
<!-- //js -->

<!--menu script-->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/emp/modernizr-2.6.2.min.js"></script>
<script src="${pageContext.request.contextPath}/js/emp/bootstrap.min.js"></script>

<!--Navbar-->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/emp/main.js"></script>
<!--//Navbar-->
<!--gallery -->
<script src="${pageContext.request.contextPath}/js/emp/jquery.chocolat.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/emp/chocolat.css" type="text/css" media="screen">
<!--light-box-files -->
<script type="text/javascript">
    $(function () {
        $('.w3layouts_gallery_grid a').Chocolat();
    });
</script>
<!-- //gallery -->


<!-- Skill Bar js -->
<script src="${pageContext.request.contextPath}/js/emp/skill.bars.jquery.js"></script>
<script>
    $(document).ready(function(){

        $('.skillbar').skillBars({
            from: 0,
            speed: 5000,
            interval: 300,
            decimals: 0,
        });

    });
</script>
<!-- //End Skill Bar js -->
<!-- /circle-->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/emp/circles.js"></script>
<script>
    var colors = [
        ['#ffffff', '#e31de1'], ['#ffffff', '#51ffe8'],['#ffffff', '#0012bc'], ['#ffffff', '#bc000d']
    ];
    for (var i = 1; i <= 7; i++) {
        var child = document.getElementById('circles-' + i),
            percentage = 25 + (i * 10);

        Circles.create({
            id:         child.id,
            percentage: percentage,
            radius:     80,
            width:      20,
            number:   	percentage / 1,
            text:       '%',
            colors:     colors[i - 1]
        });
    }

</script>
<!-- //circle -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/emp/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/emp/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},3000);
        });
    });
</script>
<!-- start-smoth-scrolling -->

<!-- Banner slider -->
<script src="${pageContext.request.contextPath}/js/emp/responsiveslides.min.js"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager:true,
            nav:true,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });

    /*查询是否重名*/
    function selecteOne() {
        var userNumber = $("#eNumber").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/registerSelect",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"userNumber":userNumber},
            dataType : "json",
            success : function (data) {
                $("#repeat").text(data.info);
            }
        })
    }

    /*修改员工信息*/
    function updateOne() {
        var uuSex = document.getElementsByName("uSex");
        for (var i=0;i<uuSex.length;i++){
            if(uuSex[i].checked==true){
                var employeeSex = uuSex[i].value;
            }
        }
        var employeeId = $("#eId").val();
        var employeeName = $("#eName").val();
        var employeeAdr = $("#eAdr").val();
        var employeePhone = $("#ePhone").val();
        var employeeBirthday = $("#eBirthday").val();
        var sp = document.getElementById("repeat").innerText;
        if(sp==""){
            $.ajax({
                url : "${pageContext.request.contextPath}/emp/updateEmp",
                type : "post",
                contentType: "application/x-www-form-urlencoded",
                data : {"employeeName":employeeName,"employeeAdr":employeeAdr,"employeePhone":employeePhone,"employeeBirthday":employeeBirthday,"employeeSex":employeeSex,"employeeId":employeeId},
                dataType : "json",
                success : function (data) {
                    if(data.info=="修改成功"){
                        alert(data.info);
                        window.location.reload();
                    }else {
                        alert(data.info);
                    }
                }
            })
        }else {
            alert("请重新输入账号！");
        }

    }

    //发送邮件
    function sendEmailOne() {
        var uEmail = $("#eEmail").val();
        console.log(uEmail);
        $.ajax({
            url : "${pageContext.request.contextPath}/login/sendEmail",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"uEmail":uEmail},
            dataType : "json",
            success : function (data) {
                $("#sssp").text(data.info);
                if(data.info=="发送失败"){
                    alert("请仔细检查您的邮箱地址是否正确！");
                }
            }
        })
    }

    //验证
    function yz() {
        var code = $("#code").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/registerEmail",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"code":code},
            dataType : "json",
            success : function (data) {
                if(data.info=="验证成功"){
                    $("#eNumber").css({'display':'block'});
                    $("#ePwd").css({'display':'block'});
                    $("#ePwdS").css({'display':'block'});
                    document.getElementById("bu").style.display="block";
                    $("#sssp").text(data.info);
                }else {
                    alert(data.info+"，请重新验证");
                }
            }
        })
    }

    //修改账号密码
    function updateTwo() {
        var sp = $("#repeat").val();
        var employeeId = $("#eId").val();
        var employeeEmail = $("#eEmail").val();
        var employeeNumber = $("#eNumber").val();
        var employeePassword = $("#ePwd").val();
        var ePwdS = $("#ePwdS").val();
        if(ePwdS==employeePassword){
            if(sp==""){
            $.ajax({
                url : "${pageContext.request.contextPath}/emp/updateEmp",
                type : "post",
                contentType: "application/x-www-form-urlencoded",
                data : {"employeeNumber":employeeNumber,"employeePassword":employeePassword,"employeeEmail":employeeEmail,"employeeId":employeeId},
                dataType : "json",
                success : function (data) {
                    if(data.info=="修改成功"){
                        alert(data.info);
                        window.location.reload();
                    }else {
                        alert(data.info);
                    }
                }
            })
            }else {
                alert("请重新输入账号！");
            }
        }else {
            alert("两次密码输入不一致！");
        }
    }
</script>
</body>
</html>
