<%--
  Created by IntelliJ IDEA.
  User: 13197
  Date: 2021/4/12
  Time: 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>会员管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <script src="${pageContext.request.contextPath}/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body onload="memberList()">
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title" align="center">查询</h4>
        </div>
        <div class="panel-body">
            <form action="" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group m-b-15">
                            <label for="input1" class="control-label col-xs-5">员工编号：</label>

                            <div class="col-xs-7">
                                <input type="text" id="input1" class="form-control" placeholder="请输入员工编号"/>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group m-b-15">
                            <label for="input2" class="control-label col-xs-5">员工名称：</label>

                            <div class="col-xs-7">
                                <input type="text" id="input2" class="form-control" placeholder="请输入员工名称"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group m-b-15">
                            <label for="input2" class="control-label col-xs-5">员工地址：</label>

                            <div class="col-xs-7">
                                <input type="text" id="input3" class="form-control" placeholder="请输入员工地址"/>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6 col-md-4 pull-right text-right">
                        <button class="btn btn-primary" onclick="memberList()" type="button" id="btntext"><i class="fa fa-search"></i> 查询</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default" >
        <div class="panel-heading">
            <h4 class="panel-title"style="display: inline-block">员工列表</h4>
            <button type='button' class="btn btn-success btn-sm" onclick='addUserInfo(this)' style="display: inline-block;margin-left: 87%">添加</button>
        </div>
        <span style="color:red" id="info"></span>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-condensed">
                    <thead>
                    <tr>
                        <th>员工编号</th>
                        <th>员工账号</th>
                        <th>姓名</th>
                        <th>地址</th>
                        <th>生日</th>
                        <th>性别</th>
                        <th>手机号码</th>
                        <th>邮箱</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="tbodyAdd">
                    </tbody>
                </table>
                <div>
                    <div class="dataTables_info pull-left" id="table1_info" role="status" aria-live="polite">每页显示<span id="row"></span>行 / 共<span id="totalPages"></span>页 / 共<span id="totalRows"></span>条数据</div>
                    <div style="padding-left: 87%">
                        <button class="btn btn-success btn-xs" onclick="preOnePage()">上一页</button>
                        <button class="btn btn-success btn-xs" onclick="nextOnePage()">下一页</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--修改的模态窗--%>
<div class="modal fade" id="updateModal" style="top:20px">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--头部-->

            <div class="modal-header" style="background-color: red; height: 20px;">
                <button class="close" data-dismiss="modal"><span class="text-primary">×</span></button>
            </div>

            <div class="modal-body">
                <table class="table" border="0">
                    <tr style="display: none" id="showId">
                        <td>账号</td><span id="repeat" style="color: red"></span>
                        <td><input type="text" id="eId" onchange="selecteTwo()"/></td>
                    </tr>
                    <tr style="display: none" id="showPwd">
                        <td>密码</td>
                        <td><input type="text" id="ePwd"/></td>
                    </tr>
                    <tr>
                        <td>姓名</td>
                        <td><input type="text" id="eName"></td>
                    </tr>
                    <tr>
                        <td>地址</td>
                        <td><input type="text" id="eAdr"></td>
                    </tr>
                    <tr>
                        <td>生日</td>
                        <td><input type="date" id="eBirthday" ></td>
                    </tr>
                    <tr>
                        <td>性别</td>
                        <td><input type="text" id="eSex"></td>
                    </tr>
                    <tr>
                        <td>手机号码</td>
                        <td><input type="text" id="ePhone" maxlength="11"></td>
                    </tr>
                    <tr>
                        <td>邮箱</td>
                        <td><input type="text" id="eEmail"></td>
                    </tr>
                    <tr id="updateInfo">
                        <td><input type="button" class="btn btn-success btn-sm" style="margin-left:125%" value="修改" onclick="update()"></td>
                        <td >
                            <button class="close" data-dismiss="modal"><span class="text-warning">取消</span></button>
                        </td>
                    </tr>
                    <tr style="display: none" id="addInfo">

                        <td><span style="color:red" id="info2"></span></td>
                        <td><button type="button" onclick="addUser()" class="btn btn-success btn-sm" style="margin-left:-10%">确定</button></td>
                        <td>
                            <button class="close" data-dismiss="modal"><span class="text-warning">取消</span></button>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //默认页面是第一页
    var page=1;
    var row = 5;
    var totalPage=0;

    //上一页
    function preOnePage() {
        page--;
        if (page<1){
            page=1;
        }
        memberList();
    }

    //下一页
    function nextOnePage() {
        page++;
        if (page>totalPage){
            page=totalPage;
        }
        memberList();
    }

    // 查询会员的列表
    function memberList() {
        var  input1 = $("#input1").val();
        var input2 = $("#input2").val();
        var input3 = $("#input3").val();
        console.log("你"+input1+"2");
        console.log("你"+input2+"2");
        console.log("你"+input3+"2");
        console.log(page);
        $.ajax({
            url: "${pageContext.request.contextPath}/emp/employeeList",
            type: "get",
            data: {
                "employeeId":input1,
                "employeeName": input2,
                "employeeAdr":input3,
                "page":page,
                "row":row
            },
            dataType: "json",
            contentType: "application/x-www-form-urlencoded;application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                var html = "";
                if (data.info == "您查询的数据不存在！") {
                    $("#tbodyAdd").html(html);
                } else {
                    for (let i = 0; i < data.info.length; i++) {
                        html += "<tr>" +
                            "<td>" + data.info[i].employeeId + "</td>" +
                            "<td>" + data.info[i].employeeNumber + "</td>" +
                            "<td>" + data.info[i].employeeName + "</td>" +
                            "<td>" + data.info[i].employeeAdr + "</td>" +
                            "<td>" + data.info[i].employeeBirthday + "</td>" +
                            "<td>" + data.info[i].employeeSex + "</td>" +
                            "<td>" + data.info[i].employeePhone + "</td>" +
                            "<td>" + data.info[i].employeeEmail + "</td>" +
                            "<td>" +
                            "<button type='button' class=\"btn btn-success btn-sm\" onclick='updateUserList(this) ' style='margin-left: 15%'>修改</button>" +
                            "<button type='button' class=\"btn btn-success btn-sm\" onclick='delUser(this)' style='margin-left: 5%'>删除</button>" +
                            "</td>" +
                            "</tr>";
                    }
                    $("#tbodyAdd").html(html);
                    totalPage = data.totalPage;
                    //显示总页数
                    $("#totalPages").text(data.totalPage);
                    //显示每页的条数
                    $("#row").text(row);
                    //显示总条数
                    $("#totalRows").text(data.total);
                }
            }
        });
    }



    //显示修改会员信息的弹窗
    function updateUserList(obj) {
        //显示弹窗
        $("#updateModal").modal("show");
        $("#updateInfo").show();
        $("#addInfo").hide();
        $("#showId").hide();
        $("#showPwd").hide();

        var eId = $(obj).parent().parent().find("td").eq(0).text();
        $("#eId").val(eId);
        var eName = $(obj).parent().parent().find("td").eq(2).text();
        $("#eName").val(eName);
        var eAdr = $(obj).parent().parent().find("td").eq(3).text();
        $("#eAdr").val(eAdr);
        var eBirthday = $(obj).parent().parent().find("td").eq(4).text();
        $("#eBirthday").val(eBirthday);
        var eSex = $(obj).parent().parent().find("td").eq(5).text();
        $("#eSex").val(eSex);
        var ePhone = $(obj).parent().parent().find("td").eq(6).text();
        $("#ePhone").val(ePhone);
        var eEmail = $(obj).parent().parent().find("td").eq(7).text();
        $("#eEmail").val(eEmail);
    }

    //修改会员信息
    function update() {

         var val = $("#eId").val();
        var val2 = $("#eName").val();
        var val3 = $("#eBirthday").val();
        var val4 = $("#eSex").val();
        var  val1 = $("#ePhone").val();
        var val5 = $("#eEmail").val();
        var val6 = $("#eAdr").val();

        $.ajax({
            url: "${pageContext.request.contextPath}/emp/updateEmp",
            type: "get",
            data: {
                 "employeeId":val,
                "employeeName": val2,
                "employeeBirthday":val3,
                "employeeSex":  val4,
                "employeePhone": val1,
                "employeeEmail":val5,
                "employeeAdr": val6,
            },
            dataType: "json",
            contentType:"application/json; charset=utf-8",
            success: function (data) {

                if (data.info=="修改成功"){
                    console.log(data);
                    $("#info").text("修改成功！");
                    window.location.reload();
                }else {
                    $("#info").text("修改失败");
                    $("#updateModal").modal("hide");
                }
            }
        });
    }

    //弹出添加会员的模态窗
    function addUserInfo() {
        //显示弹窗内容
        $("#updateModal").modal("show");
        $("#updateInfo").hide();
        $("#addInfo").show();
        $("#showId").show();
        $("#showPwd").show();
        //清空弹窗内容
        $("#eId").val(null);
        $("#eName").val(null);
        $("#eAdr").val(null);
        $("#eBirthday").val(null);
        $("#eSex").val(null);
        $("#ePhone").val(null);
        $("#eEmail").val(null);
    }
    //添加会员
    function addUser() {
        var sp = $("#repeat").text();
        console.log(sp);
        if(sp==""){
            $.ajax({
                url:"${pageContext.request.contextPath}/emp/addEmp",
                type:"get",
                data: {"employeeNumber":$("#eId").val(),"employeePassword":$("#ePwd").val(),"employeeName":$("#eName").val(),"employeeAdr":$("#eAdr").val(),"employeeSex":$("#eSex").val(),"employeeBirthday":$("#eBirthday").val(),"employeePhone":$("#ePhone").val(),"employeeEmail":$("#eEmail").val()},
                dataType:"json",
                success:function (data) {
                    if (data.info=="添加成功"){
                        $("#info").text("添加成功");
                        window.location.reload();
                    }else {
                        $("#info").text("添加失败！");
                    }
                }
            });
        }else {
            alert("请重新输入账号！");
        }
    }
    //删除用户
    function delUser(obj) {
        var userOption = confirm("你确定要删除吗？");
        if (userOption == true){
            var uId = $(obj).parent().parent().find("td").eq(0).text();
            $.ajax({
                url:"${pageContext.request.contextPath}/emp/delEmp",
                type:"get",
                data:{"employeeId":uId},
                date:"json",
                success:function (data) {
                    if (data.info=="删除成功！"){
                        $("#info").text("删除成功");
                        window.location.reload();
                    }else {
                        $("#info").text("删除失败！");
                    }
                }
            });
        }

    }


    //验证是否重名
    function selecteTwo() {
        console.log("触发事件");
        var userNumber = $("#eId").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/registerSelect",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"userNumber":userNumber},
            dataType : "json",
            success : function (data) {
                $("#repeat").text(data.info);
            }
        })
    }
</script>
</body>
</html>
