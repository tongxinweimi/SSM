<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2021-4-14
  Time: 1:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>桌子信息</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <!-- 先引入jquery在引入bootstrap，因为bootstrap是基于jquery的 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>
<body onload = "loadData()">
<br>
<from>
    <div class="form-group">
        <label class="col-md-2" align="right">编号：</label>
        <div class="col-md-2">
            <input type="text" id="deskId" class="form-control">
        </div>
        <label class="col-md-2" align="right">座位数：</label>
        <div class="col-md-2">
            <input type="text" id="seating" class="form-control">
        </div>
        <input type="button" class="layui-btn" onclick="loadData()" value="查询">
    </div>
</from>
<input type="button" class="layui-btn" onclick="batchDelete()" value="批量删除">
<input type="button" class="layui-btn" onclick="insertDesk()" value="添加">
<table class="table" >
    <thead>
    <th><input type="checkbox" name="checkedId" onclick="checkedAll(this)">全选</th>
    <th>编号</th>
    <th onclick="orderBy(this,'desk_capacity')">座位数 <span>↑</span></th>
    <th>操作</th>
    </thead>
    <tbody id="table">

    </tbody>
    <%--        <span id="message" style="color:red "></span>--%>
</table>
<div>
    <input type="button" class="layui-btn" onclick="perPage()" value="上一页">
    <input type="button" class="layui-btn" onclick="nextPage()" value="下一页">
    一共有<span id="total"></span>张桌子，共有<span id="totalPage"></span>页。当前第<span id="thisPage"></span>页。
</div>
</body>

<%-- 添加模态框 --%>

<div class="modal fade" id="addWindow" style="top:200px">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--头部-->
            <div class="modal-header" style="background-color: green; height: 20px;">
            </div>
            <div class="modal-body">
                <table class="table" border="0">

                    <tr>
                        <td>桌子编号</td>
                        <td><input type="text" id="addWindowDeskId"></td>
                    </tr>
                    <tr>
                        <td>座位数</td>
                        <td><input type="text" id="addWindowSeating"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="button" onclick="addDeskBywindow()" class="layui-btn">添加</button>
                            <button type="button" onclick="closeAddDeskwindow()" class="layui-btn">关闭当前窗口</button>
                        <td><span style="color:red" id="message"></span></td>
                        <br>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>

<%-- 修改模态框 --%>

<div class="modal fade" id="window" style="top:200px">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--头部-->
            <div class="modal-header" style="background-color: green; height: 20px;">
            </div>
            <div class="modal-body">
                <table class="table" border="0">

                    <tr>
                        <td>桌子编号</td>
                        <td><input type="text" id="windowDeskId"></td>
                    </tr>
                    <tr>
                        <td>座位数</td>
                        <td><input type="text" id="windowSeating"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="button" onclick="updateDeskBywindow()" class="layui-btn">修改</button>
                            <button type="button" onclick="closeupdateDeskwindow()" class="layui-btn">关闭当前窗口</button>
                        <td><span style="color:red" id="addMessage"></span></td>
                        <br>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    //第一次加载在第一页
    var page = 1;
    //每页的数据为8条
    var row = 8;
    //根据座位数排序
    var colName = "desk_capacity";
    //初始默认升序排序
    var order = "";
    //下一页
    function nextPage() {
        page++;
        if(page > totalPage){
            page = totalPage;
        }
        loadData();
    }
    //上一页
    function perPage() {
        page--;
        if(page < 1){
            page = 1;
        }
        loadData();
    }
    //加载数据
    function loadData() {
        //获取管理员输入的桌子ID
        var deskId = $("#deskId").val();
        //获取管理员输入的座位数ID
        var seating = $("#seating").val();
        console.log(deskId);
        console.log(seating);
        //发送ajax请求
        $.ajax({
            //发送的目的地
            url:"${pageContext.request.contextPath}/desk/ajaxSelectDesk",
            //以什么方式发送
            type:"get",
            //有无参数
            data:{"deskId":deskId,"deskCapacity":seating,"page":page,"row":row,"colName":colName,"order":order},
            //发送方式是get的话不用加contentType，是post的话得加contentType
            // contentType: "application/x-www-form-urlencoded",
            //返回的数据类型是什么
            dataType:"json",
            //发送成功的表现是什么（回调函数实现）
            success:function (data) {
                console.log(data);
                //判断后端是否查询到数据
                if(typeof(data.message) == 'string'){
                    //没有查询到数据将表格数据清空
                    $("#table").html("<span style=\"color:red \">该类型数据不存在！</span>");
                }else{
                    //取出总页数
                    totalPage = data.allPage;
                    //显示总条数
                    $("#total").text(data.total);
                    //显示总页数
                    $("#totalPage").text(data.allPage);
                    //显示当前页数
                    $("#thisPage").text(data.thisPage);
                    var html = '';
                    for(var i=0;i<data.message.length;i++){
                        html += "<tr>" +
                            "<td><input  type=\"checkbox\" name=\"checkedId\" value='"+data.message[i].deskId+"'></td>" +
                            "<td>" + data.message[i].deskId + "</td>" +
                            "<td>" + data.message[i].deskCapacity + "</td>" +
                            "<td><button type='button' class=\"layui-btn\" onclick='windowUpdateDesk(this)'>修改</button></td>" +
                            "<td><button type='button' class=\"layui-btn\" onclick='deleteDeskById(this)'>删除</button></td>" +
                            "</tr>"
                    }
                    //将数据显示在页面中
                    $("#table").html(html);
                }
            }
        });
    }

    //打开添加的窗口
    function insertDesk() {
        //打开模态框
        $("#addWindow").modal("show");
        //每次打开将提示信息清空
        $("#addMessage").text("");
    }
    //通过窗口添加桌子数据
    function addDeskBywindow() {
        //获得窗口中输入的数据
        var deskId = $("#addWindowDeskId").val();
        var deskCapacity = $("#addWindowSeating").val();
        console.log(deskId)
        console.log(deskCapacity)
        //发送ajax请求
        $.ajax({
            //传输的数据要发往哪里
            url:"${pageContext.request.contextPath}/desk/ajaxAddDeskByDesk",
            //以什么方式发送
            type:"get",
            //是否有参数
            data: {"deskId":deskId,"deskCapacity":deskCapacity},
            //返回来的数据是什么
            dataType:"json",
            //成功返回的表现是什么
            success:function(data){
                console.log(data);
                alert(data.message);
                $("#message").text(data.message);
                if(data.message == "新增成功！"){
                    //新增成功就关闭弹窗
                    $("#window").modal("hide");
                    //并刷新新增后的数据
                    loadData();
                }
            }
        });
    }
    //关闭添加弹窗
    function closeAddDeskwindow(){
        //关闭弹窗
        $("#addWindow").modal("hide");
    }

    //删除
    function deleteDeskById(obj) {
        //获得当前的桌子ID
        var deskId = $(obj).parent().parent().find("td").eq(1).text();
        console.log(deskId);
        $.ajax({
            //发送的目的地
            url:"${pageContext.request.contextPath}/desk/ajaxDeleteDeskById",
            //以什么方式发送
            type:"get",
            //有无参数
            data:{"deskId":deskId},
            //发送方式是get的话不用加contentType，是post的话得加contentType
            // contentType: "application/x-www-form-urlencoded",
            //返回的数据类型是什么
            dataType:"json",
            //发送成功的表现是什么（回调函数实现）
            success:function (data) {
                console.log(data);
                if(data.message == "删除成功！"){
                    loadData();
                }
            }
        });
    }

    //模态框修改
    function windowUpdateDesk(thisDesk) {
        //打开模态框
        $("#window").modal("show");
        //每次打开将提示信息清空
        $("#message").text("");
        var deskId = $(thisDesk).parent().parent().find("td").eq(1).text();
        //把获取到的值赋值给模态框里的信息框
        $("#windowDeskId").val(deskId);
        //获取座位数
        var deskCapacity = $(thisDesk).parent().parent().find("td").eq(2).text();
        //把获取到的值赋值给模态框里的信息框
        $("#windowSeating").val(deskCapacity);
    }
    //修改数据
    function updateDeskBywindow (){
        //从模态框中获得数据
        var deskId = $("#windowDeskId").val();
        var deskCapacity = $("#windowSeating").val();
        //发送ajax请求
        $.ajax({
            //传输的数据要发往哪里
            url:"${pageContext.request.contextPath}/desk/ajaxUpdateDeskByDesk",
            //以什么方式发送
            type:"get",
            //是否有参数
            data: {"deskId":deskId,"deskCapacity":deskCapacity},
            //返回来的数据是什么
            dataType:"json",
            //成功返回的表现是什么
            success:function(data){
                console.log(data);
                alert(data.message);
                $("#message").text(data.message);
                if(data.message == "修改成功！"){
                    //修改成功就关闭弹窗
                    $("#window").modal("hide");
                    //并刷新修改后的数据
                    loadData();
                }
            }
        });
    }
    //关闭修改弹窗
    function closeupdateDeskwindow(){
        //关闭弹窗
        $("#window").modal("hide");
    }

    //批量删除
    function batchDelete(obj) {
        //获取全选的值
        var arrId = '' ;
        $("input[type='checkbox']:checked").each(function () {
            arrId += $(this).val() + ",";
        })
        console.log(arrId);
        //让用户在次确认删除
        var is = confirm("确定删除吗？");
        if(is){
            //发送ajax请求
            $.ajax({
                url:"${pageContext.request.contextPath}/desk/ajaxBatchDeleteDeskByCheckId",
                //以什么方式发送
                type:"get",
                //是否有参数
                data: {"arrId":arrId},
                //返回来的数据是什么
                dataType:"json",
                //成功返回的表现是什么
                success:function(data){
                    console.log(data);
                    if(data.message == "批量删除成功！"){
                        //重新加载删除后的数据
                        loadData();
                    }
                }
            });
        }
    }
    //根据座位数排序
    function orderBy(obj,name) {
        console.log(name);
        //获取span标签中的箭头
        var v = $(obj).find("span").text();
        //选择以那列排序 name就是那列
        colName = name;
        if(v == "↑"){
            $(obj).find("span").text("↓");
            order = "desc";
        }else{
            $(obj).find("span").text("↑");
            order = "asc";
        }
        //排完重新刷新
        loadData();
    }
    //全选
    function checkedAll(obj) {
        //1 获取全选的值
        var v = $(obj).prop("checked");
        console.log(v);
        //选中所有name值为 checkedId 的复选框
        $("input[name='checkedId']").prop("checked",v);
        // $("input[name='checkedId']").prop("checked",$(obj).prop("checked"));
    }
</script>
</html>

