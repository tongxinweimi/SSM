<%--
  Created by IntelliJ IDEA.
  User: Kboss
  Date: 2021/4/13
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>世际酒店登录</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.2.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>

</head>
<body class="login-bg">

<div class="login">
    <div class="message">欢迎光临世际大酒店</div>
    <div id="darkbannerwrap"></div>

    <button id="userBtn" type="button" 	class="layui-btn" onclick="changeView(0)">用户密码登录</button>
    <button id="smsBtn" type="button" 	class="layui-btn layui-btn-primary" onclick="changeView(1)">短信验证码登录</button>

    <hr class="hr15">

    <form method="post" class="layui-form" id="user" >
        <input id="userNumber" name="username" placeholder="用户名"  type="text" lay-verify="required" class="layui-input" >
        <hr class="hr15">
        <input id="userPwd" name="password" lay-verify="required" placeholder="密码"  type="password" class="layui-input">
        <hr class="hr15">
        <input value="登录" lay-submit lay-filter="userLogin" style="width:100%;" type="button" onclick="login()">
        <hr class="hr20" >
        <div class="form-group">
            <div class="utility">
                <p class="semibold-text mb-2"><a href="${pageContext.request.contextPath}/login/registerPage" data-toggle="flip">第一次登录？</a></p>
            </div>
        </div>
    </form>

    <form method="post" class="layui-form" id="sms" style="display: none;">
        <input name="tel" id="tel" placeholder="电话"  type="tel" lay-verify="phone" class="layui-input" >
        <hr class="hr15">
        <input name="code" id="code" lay-verify="required" placeholder="验证码"  type="password" style="width:200px" class="layui-input layui-col-sm2 ">
        <div id="send">
            <button id="smsBtnn" type="button" style="margin-left: 10px;"	class="layui-btn layui-btn-xs" onclick="sendSms()" >发送验证码</button>
        </div>

        <hr class="hr15">
        <input value="登录" lay-submit lay-filter="smdLogin" style="width:100%;" type="button" onclick="phoneLogin()">
        <hr class="hr20" >
    </form>

</div>

<script>
    layui.use('form', function(){
        var form = layui.form;
        //监听提交
        form.on('submit(smdLogin)', function(data){
            // layer.alert(JSON.stringify(data.field));
            return false;
        });
    });

    function changeView(num){
        if (num==1){
            console.log(num)
            //显示短信的表单
            $("#sms").show()
            //移除白色按钮样式
            $("#smsBtn").removeClass("layui-btn layui-btn-primary")
            //添加蓝色按钮样式
            $("#smsBtn").addClass("layui-btn")

            //隐藏用户密码登录的表单
            $("#user").hide()
            //移除蓝色按钮样式
            $("#userBtn").removeClass("layui-btn")
            //添加白色色按钮样式
            $("#userBtn").addClass("layui-btn layui-btn-primary")
        }else{
            console.log(num)

            $("#user").show()
            $("#userBtn").removeClass("layui-btn layui-btn-primary")
            $("#userBtn").addClass("layui-btn")


            $("#sms").hide()
            $("#smsBtn").removeClass("layui-btn")
            $("#smsBtn").addClass("layui-btn layui-btn-primary")
        }
    }
    //计时器秒
    var s =3

    //发送手机验证码
    function sendSms(){
        //启用发送验证码
        // $("#send").html("<button type='button' class='layui-btn'>发送验证码</button>");
        var phone = $("#tel").val();
        $.ajax({
            url: "${pageContext.request.contextPath}/login/phoneLoginCode",
            type: "get",
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            data: {"phone":phone},
            success:function (data) {
                alert(data.info);
            }
        });
        // var time = setInterval(function () {
        //     //禁用发送验证码按钮
        //     $("#send").html("<button type='button'  class='layui-btn layui-btn-disabled layui-btn-small' >"+s+"秒后发送验证码</button>")
        //     s--;
        //     if(s==0){ //倒计时结束
        //         //清除计时器
        //         clearInterval(time);
        //         //启用发送验证码
        //         $("#send").html("<button type='button' class='layui-btn'>发送验证码</button>");
        //     }
        // },1000)

    }
    //手机登录
    function phoneLogin() {
        var phone = $("#tel").val();
        var phoneCode = $("#code").val();
        $.ajax({
            url: "${pageContext.request.contextPath}/login/phoneLogin",
            type: "get",
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            data: {"phone":phone,"phoneCode":phoneCode},
            success:function (data) {
                console.log(data);
                alert(data.info);
                if (data.info!="新用户您好"||data.info!="登录成功"){
                    window.location.href="${pageContext.request.contextPath}/login/foodIndex?id="+data.user.uId;
                }else  if (data.info=="恭喜你登录成功"){
                    window.location.href="${pageContext.request.contextPath}/login/indexTwo?id="+data.employee.employeeId;
                }
            }
        });
    }

    //登录
    function login() {
        var userNumber = $("#userNumber").val();
        var userPwd = $("#userPwd").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/loginShiro",
            type : "post",
            data : {"uNumber":userNumber,"uPassword":userPwd},
            contentType: "application/x-www-form-urlencoded",
            dataType : "json",
            success : function (data) {
                console.log(data);
                alert(data.info);
                if(data.info=="登录成功"){
                    var a = data.iinfo;
                    var id;
                    if(a=="user"){
                        id = data.userId;
                        window.location.href="${pageContext.request.contextPath}/login/foodIndex?Id="+id;
                    }else if(a=="emp"){
                        id = data.empId;
                        window.location.href="${pageContext.request.contextPath}/login/indexTwo?Id="+id;
                    }

                }
            }
        })
    }
</script>

</body>
</html>
