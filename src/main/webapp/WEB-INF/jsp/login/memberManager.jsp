<%--
  Created by IntelliJ IDEA.
  User: 13197
  Date: 2021/4/12
  Time: 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>会员管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <script src="${pageContext.request.contextPath}/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body onload="memberList()">
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title" align="center">查询</h4>
        </div>
        <div class="panel-body">
            <form action="" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group m-b-15">
                            <label for="input1" class="control-label col-xs-5">会员账号：</label>

                            <div class="col-xs-7">
                                <input type="text" id="input1" class="form-control" placeholder="请输入会员账号"/>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group m-b-15">
                            <label for="input2" class="control-label col-xs-5">会员名称：</label>

                            <div class="col-xs-7">
                                <input type="text" id="input2" class="form-control" placeholder="请输入会员名称"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group m-b-15">
                            <label for="input3" class="control-label col-xs-5">会员级别：</label>

                            <div class="col-xs-7">
                                <select type="text" id="input3" class="form-control">
                                    <option value="">-----请选择会员级别-----</option>
                                    <option value="钻石会员">----------钻石会员---------</option>
                                    <option value="高级会员">----------高级会员---------</option>
                                    <option value="普通会员">----------普通会员---------</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 pull-right text-right">
                    <button class="btn btn-primary" onclick="memberList()" type="button" id="btntext"><i class="fa fa-search"></i> 查询</button>
                </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default" >
        <div class="panel-heading">
            <h4 class="panel-title"style="display: inline-block">会员列表</h4>
            <button type='button' class="btn btn-success btn-sm" onclick='addUserInfo(this)' style="display: inline-block;margin-left: 87%">添加</button>
        </div>
        <span style="color:red" id="info"></span>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-condensed">
                    <thead>
                    <tr>
                        <th>会员账号</th>
                        <th>姓名</th>
                        <th>生日</th>
                        <th>性别</th>
                        <th>手机号码</th>
                        <th>邮箱</th>
                        <th>会员等级</th>
                        <th>积分</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="tbodyAdd">
                    </tbody>
                </table>
                <div>
                    <div class="dataTables_info pull-left" id="table1_info" role="status" aria-live="polite">显示<span id="row"></span>行 / 共<span id="totalPages"></span>页 / 共<span id="totalRows"></span>条数据</div>
                    <div style="padding-left: 87%">
                        <button class="btn btn-success btn-xs" onclick="preOnePage()">上一页</button>
                        <button class="btn btn-success btn-xs" onclick="nextOnePage()">上一页</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--修改的模态窗--%>
<div class="modal fade" id="updateModal" style="top:20px">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--头部-->

            <div class="modal-header" style="background-color: red; height: 20px;">
                <button class="close" data-dismiss="modal"><span class="text-primary">×</span></button>
            </div>

            <div class="modal-body">
                <table class="table" border="0">
                    <tr style="display: none" id="showId">
                        <td>用户编号</td>
                        <td><input type="text" id="uId"/></td>
                    </tr>
                    <tr style="display: none" id="showNumber">
                        <td>账号</td>
                        <td><input type="text" id="uNumber" onchange="selectee()"/></td>
                        <td> <span id="repeat" style="color: red"></span></td>
                    </tr>

                    <tr>
                        <td>姓名</td>
                        <td><input type="text" id="uName"></td>
                    </tr>
                    <tr>
                        <td>生日</td>
                        <td><input type="date" id="uBirthday" ></td>
                    </tr>
                    <tr>
                        <td>性别</td>
                        <td><input type="text" id="uSex"></td>
                    </tr>
                    <tr>
                        <td>手机号码</td>
                        <td><input type="text" id="uPhone" maxlength="11"></td>
                    </tr>
                    <tr>
                        <td>邮箱</td>
                        <td><input type="text" id="uEmail"></td>
                    </tr>
                    <tr>
                        <td>会员等级</td>
                        <td><input type="text" id="uVip"></td>
                    </tr>
                    <tr>
                        <td>积分</td>
                        <td><input type="text" id="uIntegral"></td>
                    </tr>

                    <tr id="updateInfo">
                        <td><input type="button" class="btn btn-success btn-sm" style="margin-left:125%" value="修改" onclick="update()"></td>
                        <td >
                            <button class="close" data-dismiss="modal"><span class="text-warning">取消</span></button>
                        </td>
                    </tr>
                    <tr style="display: none" id="addInfo">

                        <td><span style="color:red" id="info2"></span></td>
                        <td><button type="button" onclick="addUser()" class="btn btn-success btn-sm" style="margin-left:-10%">确定</button></td>
                        <td>
                            <button class="close" data-dismiss="modal"><span class="text-warning">取消</span></button>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //默认页面是第一页
    var page=1;
    var row = 5;
    var totalPage=0;

    //上一页
    function preOnePage() {
        page--;
        if (page<1){
            page=1;
        }
        selectProductList();
    }

    //下一页
    function nextOnePage() {
        page++;
        if (page>totalPages){
            page=totalPages;
        }
        selectProductList();
    }

    // 查询会员的列表
    function memberList() {
        var  input1 = $("#input1").val();
        var input2 = $("#input2").val();
        var input3 = $("#input3").val();
        $.ajax({
            url: "${pageContext.request.contextPath}/userController/memberList",
            type: "get",
            data: {
                "uNumber":input1,
                "uName": input2,
                "uVip":input3
            },
            dataType: "json",
            contentType: "application/x-www-form-urlencoded;application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                var html = "";
                for (let i = 0; i < data.userList.length; i++) {
                    html += "<tr>" +
                        "<td style='display: none'>" + data.userList[i].uId + "</td>" +
                        "<td>" + data.userList[i].uNumber + "</td>" +
                        "<td>" + data.userList[i].uName + "</td>" +
                        "<td>" + data.userList[i].uBirthday + "</td>" +
                        "<td>" + data.userList[i].uSex + "</td>" +
                        "<td>" + data.userList[i].uPhone + "</td>" +
                        "<td>" + data.userList[i].uEmail + "</td>" +
                        "<td>" + data.userList[i].uVip + "</td>" +
                        "<td>" + data.userList[i].uIntegral + "</td>" +
                        "<td>" +
                        "<button type='button' class=\"btn btn-success btn-sm\" onclick='updateUserList(this) ' style='margin-left: 15%'>修改</button>" +
                        "<button type='button' class=\"btn btn-success btn-sm\" onclick='delUser(this)' style='margin-left: 5%'>删除</button>" +
                        "</td>" +
                        "</tr>";
                }
                $("#tbodyAdd").html(html);
                totalPage = data.totalPages;
                //显示总页数
                $("#totalPages").text(data.totalPages);
                //显示每页的条数
                $("#row").text(row);
                //显示总条数
                $("#totalRows").text(data.totalRows);
            }
        });
    }



    //显示修改会员信息的弹窗
    function updateUserList(obj) {
        //显示弹窗
        $("#updateModal").modal("show");

        /*var uId = $(obj).parent().parent().find("td").eq(0).text();
        $("#uId").val(uId);*/
        var uName = $(obj).parent().parent().find("td").eq(2).text();
        $("#uName").val(uName);
        var uBirthday = $(obj).parent().parent().find("td").eq(3).text();

        //alert(uBirthday);
        $("#uBirthday").val(uBirthday);
        var uSex = $(obj).parent().parent().find("td").eq(4).text();
        $("#uSex").val(uSex);
        var uPhone = $(obj).parent().parent().find("td").eq(5).text();
        $("#uPhone").val(uPhone);
        var uEmail = $(obj).parent().parent().find("td").eq(6).text();
        $("#uEmail").val(uEmail);
        var uVip = $(obj).parent().parent().find("td").eq(7).text();
        $("#uVip").val(uVip);
        var uIntegral = $(obj).parent().parent().find("td").eq(8).text();
        $("#uIntegral").val(uIntegral);
    }

    //修改会员信息
    function update() {

        // var val = $("#uId").val();
        var val2 = $("#uName").val();
        var val3 = $("#uBirthday").val();
        var val4 = $("#uSex").val();
        var  val1 = $("#uPhone").val();
        var val5 = $("#uEmail").val();
        var val6 = $("#uVip").val();
        var val7 = $("#uIntegral").val();
        $.ajax({
            url: "${pageContext.request.contextPath}/userController/updateMember",
            type: "get",
            data: {
                // "uId":val,
                "uName": val2,
                "uBirthday":val3,
                "uSex":  val4,
                "uPhone": val1,
                "uEmail":val5,
                "uVip": val6,
                "uIntegral":val7
            },
            dataType: "json",
            contentType:"application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                if (data.info=="修改成功！"){
                    $("#info").text("修改成功！");
                    window.location.href="${pageContext.request.contextPath}/userController/memberManager";
                }else {
                    $("#info").text("修改失败！");
                    $("#updateModal").modal("hide");
                }
            }
        });
    }

    //弹出添加会员的模态窗
    function addUserInfo() {
        //显示弹窗内容
        $("#updateModal").modal("show");
        $("#updateInfo").hide();
        $("#addInfo").show();
        $("#showNumber").show();
    }
    //添加会员
    function addUser() {
        var userNumber = $("#uNumber").val();
        if(userNumber==""){
            alert("请重新输入账号！")
        }else {
        $.ajax({
            url:"${pageContext.request.contextPath}/userController/addUser",
            type:"get",
            data: {"uNumber":$("#uNumber").val(),"uName":$("#uName").val(),"uBirthday":$("#uBirthday").val(),"uSex":$("#uSex").val(),"uPhone":$("#uPhone").val(),"uEmail":$("#uEmail").val(),"uVip":$("#uVip").val(),"uIntegral":$("#uIntegral").val()},
            dataType:"json",
            contentType:"application/json;charset=utf-8",
            success:function (data) {
                if (data.info=="添加成功！"){
                    $("#info").text("添加成功");
                    window.location.href="${pageContext.request.contextPath}/userController/memberManager";
                }else {
                    $("#info").text("添加失败！");
                }
            }
        });
        }
    }
    //删除用户
    function delUser(obj) {
        var userOption = confirm("你确定要删除吗？");
        if (userOption == true){
            var uId = $(obj).parent().parent().find("td").eq(0).text();
            $.ajax({
                url:"${pageContext.request.contextPath}/userController/delUser",
                type:"get",
                data:{"uId":uId},
                date:"json",
                success:function (data) {
                    if (data.info=="删除成功！"){
                        $("#info").text("删除成功");
                        window.location.href="${pageContext.request.contextPath}/userController/memberManager";
                    }else {
                        $("#info").text("删除失败！");
                    }
                }
            });
        }

    }

    //验证账号是否重名
    function selectee() {
        var userNumber = $("#uNumber").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/registerSelect",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"userNumber":userNumber},
            dataType : "json",
            success : function (data) {
                $("#repeat").text(data.info);
            }
        })
    }

</script>
</body>
</html>
