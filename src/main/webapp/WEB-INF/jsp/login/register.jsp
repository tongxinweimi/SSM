<%--
  Created by IntelliJ IDEA.
  User: Kboss
  Date: 2021/4/14
  Time: 10:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>世际酒店会员注册页面</title>
</head>
<body>
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">

    <div class="login-box" style="height: 800px;width: 500px">
        <form class="login-form" style="height: 500px">
            <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>世际酒店会员注册</h3>
            <div class="form-group">
                <label class="control-label">账号</label>
                <input id="uNumber" class="form-control" type="text" placeholder="登录账号" onchange="selecte()">
                <span id="repeat" style="color: red"></span>
            </div>
            <div class="form-group">
                <label class="control-label">密码</label>
                <input id="uPwd" class="form-control" type="password" placeholder="登录密码">
            </div>
            <div class="form-group">
                <label class="control-label">请再输一次密码</label>
                <input id="uPwdS" class="form-control" type="password" placeholder="确认密码">
            </div>
            <div class="form-group">
                <label class="control-label">手机号</label>
                <input id="uPhone" class="form-control" type="tel" placeholder="手机号">
            </div>
            <div class="form-group">
                <label class="control-label">姓名</label>
                <input id="uName" class="form-control" type="text" placeholder="姓名">
            </div>
            <div class="form-group">
                <label class="control-label">性别</label>
                <div class="form-group">
                    <input name="uSex"  type="radio" value="男" >男
                    <input name="uSex"  type="radio" value="女" >女
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">生日</label>
                <input id="uBirthday" class="form-control" type="date" >
            </div>
            <div class="form-group">
                <div class="utility">
                    <p class="semibold-text mb-2"><a href="#" data-toggle="flip">账户安全？</a></p>
                </div>
            </div>
            <div class="form-group btn-container">
                <button type="button" class="btn btn-primary btn-block" onclick="registerOne()"><i class="fa fa-sign-in fa-lg fa-fw"></i>立即注册</button>
            </div>
        </form>
        <form class="forget-form" style="height: 500px">
            <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>账户安全</h3>
            <span style="color: #ffc0cb">请绑定常用邮箱，增强账户安全</span>
            <div class="form-group">
                <label class="control-label">邮箱</label>
                <input id="uEmail" class="form-control" type="email" placeholder="邮箱地址">
            </div>
            <div class="form-group btn-container">
                <button type="button" class="btn btn-primary btn-block" style="width: 100px" onclick="sendEmail()"></i>发送验证码</button><span id="ssp" style="color: palevioletred"></span>
            </div>
            <br>
            <div class="form-group">
                <input id="yzm" class="form-control" type="text" placeholder="验证码" style="width: 200px;height: 50px">
            </div>
            <div class="form-group btn-container">
                <button class="btn btn-primary btn-block" type="button" onclick="registerTwo()"></i>绑定并注册</button>
            </div>
            <div class="form-group mt-3">
                <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> 返回注册页面</a></p>
            </div>
        </form>
    </div>
</section>
<!-- Essential javascripts for application to work-->
<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="${pageContext.request.contextPath}/js/plugins/pace.min.js"></script>
<script type="text/javascript">
    // Login Page Flipbox control
    $('.login-content [data-toggle="flip"]').click(function() {
        $('.login-box').toggleClass('flipped');
        return false;
    });

    /*查询用户账号是否重复*/
    function selecte() {
        var userNumber = $("#uNumber").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/registerSelect",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"userNumber":userNumber},
            dataType : "json",
            success : function (data) {
                $("#repeat").text(data.info);
            }
        })
    }

    /*注册新用户*/
    function registerOne() {
        var uuSex = document.getElementsByName("uSex");
        for (var i=0;i<uuSex.length;i++){
            if(uuSex[i].checked==true){
            var uSex = uuSex[i].value;
            }
        }
        var uNumber = $("#uNumber").val();
        var uPwd = $("#uPwd").val();
        var uPwdS = $("#uPwdS").val();
        var uName = $("#uName").val();
        var uBierthday = $("#uBirthday").val();
        var sp = document.getElementById("repeat").innerText;
        var uEmail = $("#uEmail").val();
        var uPhone = $("#uPhone").val();
        if(uPwd==uPwdS){
            if(sp==""){
                $.ajax({
                    url : "${pageContext.request.contextPath}/login/register",
                    type : "post",
                    contentType: "application/x-www-form-urlencoded",
                    data : {"uNumber":uNumber,"uPassword":uPwd,"uName":uName,"uBirthday":uBierthday,"uSex":uSex,"uEmail":uEmail,"uPhone":uPhone},
                    dataType : "json",
                    success : function (data) {
                        if(data.info=="注册成功！"){
                            alert(data.info);
                            window.location.href="${pageContext.request.contextPath}/login/loginPage";
                        }else {
                            alert(data.info);
                        }
                    }
                })
            }else {
                alert("请重新输入账号！");
            }
        }else {
            alert("两次输入的密码不一致！");
        }
    }

    /*邮箱发送验证码*/
    function sendEmail() {
        var uEmail = $("#uEmail").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/sendEmail",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"uEmail":uEmail},
            dataType : "json",
            success : function (data) {
                $("#ssp").text(data.info);
                if(data.info=="发送失败"){
                    alert("请仔细检查您的邮箱地址是否正确！");
                }
            }
        })
    }


    /*邮箱绑定注册*/
    function registerTwo() {
        var code = $("#yzm").val();
        $.ajax({
            url : "${pageContext.request.contextPath}/login/registerEmail",
            type : "post",
            contentType: "application/x-www-form-urlencoded",
            data : {"code":code},
            dataType : "json",
            success : function (data) {
                if(data.info=="验证成功"){
                    $("#ssp").text("");
                    registerOne();
                }else {
                    alert(data.info+"，请重新验证");
                }
            }
        })
    }
</script>
</body>
</html>