<%--
  Created by IntelliJ IDEA.
  User: Kboss
  Date: 2021/4/12
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script src="${pageContext.request.contextPath}\js\jquery-3.2.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    <link href="${pageContext.request.contextPath}/css/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <link href="${pageContext.request.contextPath}/js/bootstrap.js" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <!-- Custom Theme files -->
    <link href="${pageContext.request.contextPath}/css/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <%--<link href='http://fonts.useso.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.useso.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>--%>
    <!--Animation-->
    <script src="${pageContext.request.contextPath}/js/wow.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/animate.css" rel='stylesheet' type='text/css' />
    <script>
        new WOW().init();
    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/move-top.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
</head>
<body onload="loadData()">
    <div  class="header">
        <a href="${pageContext.request.contextPath}/login/loginPage">${user.uName}</a>
        <div class="x-body">
            <div class="layui-row">
                <form class="layui-form layui-col-md12 x-so">
                    <input type="text"id="selectFootName" placeholder="${user.uName}" autocomplete="off" class="layui-input">
                    <input type="text"id="selectFootType" placeholder="请输入菜品类型" autocomplete="off" class="layui-input">
                    <button type="button" style="width: 60px" class="layui-btn"  onclick="loadData()"><i class="layui-icon">&#xe615;</i></button>
                    <input type="button" class="layui-btn"  onclick="submitOrder()" value="订单">
                </form>
            </div>

            <div id="tb" style="margin-left: 20%;margin-right: 20%"></div>

            <div class="page">
                <div>
                    <button type="button" class="btn btn-success btn-sm" onclick="pres()">上一页</button>
                    <button type="button" class="btn btn-success btn-sm" onclick="nexts()">下一页</button>
                    一共有<span id="total"></span>数据，共有<span id="totalPage"></span>页
                </div>
            </div>

        </div>
    </div>
    <script>
        //默认页面是第一页
        var page=1;
        var row = 5;
        var totalPage=0;
        var colName="food_id";
        var order ="asc";
        var prices = new Array();
        var foodprice=0;
        //上一页
        function pres() {
            page--;
            if(page < 1){
                page = 1;
            }
            loadData();
        }
        //下一页
        function nexts() {
            page++;
            if(page > totalPage){
                page = totalPage;
            }
            loadData();
        }
        //加载数据
        function loadData() {
            var foodName = $("#selectFootName").val();
            var foodType = $("#selectFootType").val();
            $.ajax({
                url:"${pageContext.request.contextPath}/food/foodList",
                type:"get",
                data:{"foodName":foodName,"foodType":foodType,"page":page,"row":row,"colName":colName,"order":order},
                dataType:"json",
                success:function (data) {
                    console.log(data);
                    var html = "";
                    totalPage = data.totalPage;
                    //显示总条数
                    $("#total").text(data.total);
                    //显示总页数
                    $("#totalPage").text(totalPage);
                    for(var i=0;i<data.info.length;i++){
                        html +=
                        "<div class=\"Popular-Restaurants-grid wow fadeInLeft\" data-wow-delay=\"0.4s\">"+
                            "<div class=\"col-md-3 restaurent-logo\">"+
                                "<img src=\"${pageContext.request.contextPath}/images/cuisine6.jpg\" class=\"img-responsive\"/>"+
                            "</div>"+
                            "<div style=\"display:none\">"+
                            "<span>"+data.info[i].foodId+"</span>"+
                            "</div>"+
                            "<div class=\"col-md-2 restaurent-title\">"+
                                "<div class=\"logo-title logo-title-1\">"+
                                    "<h4><a>"+data.info[i].foodName+"</a></h4>"+
                                "</div>"+
                                "<div class=\"rating\">"+
                                    "<span>类型</span>"+
                                    "<a>"+data.info[i].foodType+"</a>"+
                                "</div>" +
                            "</div>"+
                            "<div class=\"col-md-7 buy\">" +
                                "<span>$"+data.info[i].foodPrice+"</span>"+
                                "<input type=\"button\" onclick='addOrder(this)' value=\"加入订单\">" +
                            "</div>"+
                            "<div class=\"clearfix\"></div>"+
                        "</div>"
                    }
                    $("#tb").html(html);
                    totalPage = data.totalPage;
                }
            })
        }
        var foodId = 0;
        function member_del(obj,id){
            layer.confirm('确认要删除吗？',function(index){
                //发异步删除数据
                $(obj).parents("tr").remove();
                layer.msg('已删除!',{icon:1,time:1000});
            });
        }
        //添加订单
        function addOrder(obj) {
            var id = $(obj).parent().parent().find("div").eq(1).text();
            var price = $(obj).parent().parent().find("div").eq(5).text();
            foodId+=id;
            foodprice+=price;
            prices =foodprice.split("$");
            alert("添加成功");
        }
        //提交订单
        function submitOrder() {
            var sum = eval(prices.join('+'));
            var payType="未支付";
            console.log(foodId)
            $.ajax({
                url:"${pageContext.request.contextPath}/foodOrder/addFoodOrder",
                type: "get",
                data:{
                    "userId":${user.uId},
                    "price":sum,
                    "payType":payType,
                    "foodIds":foodId
                },
                dataType:"json",
                success:function (data){
                    console.log(data);
                    window.location.href="${pageContext.request.contextPath}/login/openUI?uId="+${user.uId};
                }
            })
            console.log("sum"+sum);
        }
    </script>
</body>

