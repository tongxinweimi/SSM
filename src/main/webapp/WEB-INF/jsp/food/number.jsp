<%--
  Created by IntelliJ IDEA.
  User: Fantasy
  Date: 2021/4/14
  Time: 21:44
  To change this template use File | Settings | File Templates.
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script src="${pageContext.request.contextPath}\js\jquery-3.2.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    <link href="${pageContext.request.contextPath}/css/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <link href="${pageContext.request.contextPath}/js/bootstrap.js" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <!-- Custom Theme files -->
    <link href="${pageContext.request.contextPath}/css/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <%--<link href='http://fonts.useso.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.useso.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--Animation-->--%>
    <script src="${pageContext.request.contextPath}/js/wow.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/animate.css" rel='stylesheet' type='text/css' />
    <script>
        new WOW().init();
    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/move-top.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
</head>
<body onload="loadData()">
    <div  class="header">

        <div id="tb" style="margin-left: 20%;margin-right: 20%"></div>
        <div class="page">
             <div>
                 <button type="button" class="btn btn-success btn-sm" onclick="pay()">结账</button>
             </div>
        </div>
    </div>
    <script>
        function loadData() {
            console.log(${User.uId});
            $.ajax({
                url:"${pageContext.request.contextPath}/orderNumber/orderNumberList",
                type:"get",
                data:{"uId":${User.uId}},
                dataType:"json",
                success:function (data) {
                    console.log(data);
                    var html = "";
                    for(var i=0;i<data.info.length;i++){
                        html +=
                            "<div class=\"Popular-Restaurants-grid wow fadeInLeft\" data-wow-delay=\"0.4s\">"+
                            "<div class=\"col-md-3 restaurent-logo\">"+
                            "<img src=\"${pageContext.request.contextPath}/images/cuisine6.jpg\" class=\"img-responsive\"/>"+
                            "</div>"+
                            "<div style=\"display:none\">"+
                                "<input type=\"hidden\" value=\"\">"+
                            "<span>"+data.info[i].foodId+"</span>"+
                            "</div>"+
                            "<div class=\"col-md-2 restaurent-title\">"+
                            "<div class=\"logo-title logo-title-1\">"+
                            "<h4><a>"+data.info[i].foodId+data.food[i].foodName+"</a></h4>"+
                            "</div>"+
                            "<div class=\"rating\">"+
                            "<span>数量</span>"+
                            "<a>"+data.info[i].foddNumber+"</a>"+
                            "</div>" +
                            "</div>"+
                            "<div class=\"col-md-7 buy\">" +
                            "<span>"+data.info[i].type+"</span>"+
                            "</div>"+
                            "<div class=\"clearfix\"></div>"+
                            "</div>"
                    }
                    console.log(data.foodOrder);
                    html +="<span>合计："+data.foodOrder.price+"</span>";
                    html +="<form action=\"${pageContext.request.contextPath}/pay\">\n" +
                        "\n" +
                        "  <input type=\"hidden\" name=\"WIDout_trade_no\" value="+data.foodOrder.foodOrderId+">\n" +
                        "  <input type=\"hidden\" name=\"WIDsubject\" value=\"订单\">\n" +
                        "  <input type=\"hidden\" name=\"WIDtotal_amount\" value="+data.foodOrder.price+">\n" +
                        "  <input type=\"hidden\" name=\"WIDbody\" value=\"\">\n" +
                        "  <button type=\"submit\" class=\"cancel-order\">支付</button>\n" +
                        "  </form>";
                    $("#tb").html(html);
                }
            })
        }
        function member_del(obj,id){
            layer.confirm('确认要删除吗？',function(index){
                //发异步删除数据
                $(obj).parents("tr").remove();
                layer.msg('已删除!',{icon:1,time:1000});
            });
        }
        function pay() {
            $.ajax({
                url:"${pageContext.request.contextPath}/pay",
                type:"get",
                data:{"userId":${User.uId}},
                dataType:"json",
                success:function (data) {
                    loadData();
                }
            })
        }
    </script>
</body>

</html>
