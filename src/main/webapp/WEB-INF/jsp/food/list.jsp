<%--
  Created by IntelliJ IDEA.
  User: Fantasy
  Date: 2021/4/12
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head >
    <base href="<%=basePath%>"/>
    <title>Title</title>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script src="${pageContext.request.contextPath}\js\jquery-3.2.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</head>
<body onload="loadData()">
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so">
            <input type="text"id="selectFootName" placeholder="请输入菜品名" autocomplete="off" class="layui-input">
            <input type="text"id="selectFootType" placeholder="请输入菜品类型" autocomplete="off" class="layui-input">
            <input type="button" class="layui-btn"  onclick="loadData()"><i class="layui-icon">&#xe615;</i></input>
        </form>
    </div>
    <xblock>
        <button class="layui-btn" onclick="add()"><i class="layui-icon"></i>添加</button>
        <span class="x-right" style="line-height:40px">共有数据：1 条</span>
    </xblock>
    <table class="layui-table">
        <thead>
        <tr>
            <th>
                <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>菜品编号</th>
            <th>菜品名称</th>
            <th>菜品分类</th>
            <th>菜品价格</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody id="tb">

        </tbody>
    </table>
    <div class="page">
        <div>
            <button type="button" class="btn btn-success btn-sm" onclick="pres()">上一页</button>
            <button type="button" class="btn btn-success btn-sm" onclick="nexts()">下一页</button>
            一共有<span id="total"></span>数据，共有<span id="totalPage"></span>页
        </div>
    </div>
</div>
<%--添加模态框--%>
<div class="modal fade" id="add" style="top:200px">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--头部-->
            <div class="modal-header" style="background-color: green; height: 20px;">
            </div>
            <div class="modal-body">
                <table class="table" border="0">
                    <tr>
                        <td>菜品名称</td>
                        <input type="hidden" id="foodId">
                        <td><input type="text" id="foodName"></td>
                    </tr>
                    <tr>
                        <td>菜品分类</td>
                        <td><input type="text" id="foodType"></td>
                    </tr>
                    <tr>
                        <td>菜品价格</td>
                        <td><input type="text" id="foodPrice"></td>
                    </tr>
                    <tr>
                        <td><span style="color:red" id="info"></span></td>
                        <td colspan="2">
                            <button type="button" onclick="addFood()" class="btn btn-success btn-sm">添加</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<%--修改模态框--%>
<div class="modal fade" id="updata" style="top:200px">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--头部-->
            <div class="modal-header" style="background-color: green; height: 20px;">
            </div>
            <div class="modal-body">
                <table class="table" border="0">
                    <tr>
                        <td>菜品名称</td>
                        <input type="hidden" id="foodIdEd">
                        <td><input type="text" id="foodNameEd"></td>
                    </tr>
                    <tr>
                        <td>菜品分类</td>
                        <td><input type="text" id="foodTypeEd"></td>
                    </tr>
                    <tr>
                        <td>菜品价格</td>
                        <td><input type="text" id="foodPriceEd"></td>
                    </tr>
                    <tr>
                        <td><span style="color:#ff0000" id="infoEd"></span></td>
                        <td colspan="2">
                            <button type="button" onclick="updataFood()" class="btn btn-success btn-sm">修改</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //默认页面是第一页
    var page=1;
    var row = 5;
    var totalPage=0;
    var colName="food_id";
    var order ="asc";
    //上一页
    function pres() {
        page--;
        if(page < 1){
            page = 1;
        }
        loadData();
    }
    //下一页
    function nexts() {
        page++;
        if(page > totalPage){
            page = totalPage;
        }
        loadData();
    }
    //加载数据
    function loadData() {
        var foodName = $("#selectFootName").val();
        var foodType = $("#selectFootType").val();
        $.ajax({
            url:"${pageContext.request.contextPath}/food/foodList",
            type:"get",
            data:{"foodName":foodName,"foodType":foodType,"page":page,"row":row,"colName":colName,"order":order},
            dataType:"json",
            success:function (data) {
                console.log(data);
                totalPage = data.totalPage;
                //显示总条数
                $("#total").text(data.total);
                //显示总页数
                $("#totalPage").text(totalPage);
                var html = "";
                for(var i=0;i<data.info.length;i++){
                    html += "<tr>" +
                        "<td><input  type=\"checkbox\" name=\"selectId\" value='"+data.info[i].foodId+"'></td>"+
                        "<td>"+data.info[i].foodId+"</td>" +
                        "<td>"+data.info[i].foodName+"</td>" +
                        "<td>"+data.info[i].foodType+"</td>" +
                        "<td>"+data.info[i].foodPrice+"</td>" +
                        "<td>" +
                        "<button type='button' class=\"btn btn-success btn-sm\" onclick='updata(this)'>修改</button>" +
                        "<button type='button' class=\"btn btn-success btn-sm\" onclick='delFood(this)'>删除</button>" +
                        "</td>" +
                        "</tr>";
                }
                $("#tb").html(html);
                totalPage = data.totalPage;
            }
        })
    }
    /*用户-删除*/
    function member_del(obj,id){
        layer.confirm('确认要删除吗？',function(index){
            //发异步删除数据
            $(obj).parents("tr").remove();
            layer.msg('已删除!',{icon:1,time:1000});
        });
    }
    function add() {
        //显示弹窗
        $("#add").modal("show");
        //清空提示信息
        $("#info").text("");
    }
    function addFood() {
        var foodName = $("#foodName").val() ;
        var foodType = $("#foodType").val();
        var foodPrice = $("#foodPrice").val();
        $.ajax({
            url:"${pageContext.request.contextPath}/food/addFood",
            type: "get",
            data: {"foodName":foodName,"foodType":foodType,"foodPrice":foodPrice},
            success:function (data) {
                //关闭弹窗
                $("#add").modal("hide");
                //刷新
                loadData();
            }
        })
    }
    function updata(obj) {
        //显示弹窗
        $("#updata").modal("show");
        //清空提示信息
        $("#infoEd").text("");
        var id = $(obj).parent().parent().find("td").eq(1).text();
        $("#foodIdEd").val(id);
        var name = $(obj).parent().parent().find("td").eq(2).text();
        $("#foodNameEd").val(name);
        var type = $(obj).parent().parent().find("td").eq(3).text();
        $("#foodTypeEd").val(type);
        var price = $(obj).parent().parent().find("td").eq(4).text();
        $("#foodPriceEd").val(price);
    }
    function updataFood() {
        var foodId = $("#foodIdEd").val();
        var foodName = $("#foodNameEd").val();
        var foodType = $("#foodTypeEd").val();
        var foodPrice = $("#foodPriceEd").val();
        $.ajax({
            url:"${pageContext.request.contextPath}/food/updataFood",
            type:"get",
            data:{foodId:foodId,foodName:foodName,foodPrice:foodPrice,foodType:foodType},
            dataType: "json",
            success:function (data) {
                $("#infoEd").text(data.info);
                if(data.info=="修改成功"){
                    //关闭弹窗
                    $("#updata").modal("hide");
                    //刷新
                    loadData();
                }
            }
        })
    }
    function delFood(obj) {
        var is = confirm("你确定要删除吗");
        if(is){
            //要删除的id
            var id = $(obj).parent().parent().find("td").eq(1).text();
            //删除ajax
            $.ajax({
                url:"${pageContext.request.contextPath}/food/delFood",
                type:"get",
                data:{foodId:id},
                dataType: "json",
                success:function (data) {
                    if(data.info=="删除成功"){
                        loadData();
                    }
                }
            })
        }
    }
    function selectFood() {
        var foodName = $("#selectFootName").val();
        var foodType = $("#selectFootType").val();
        $.ajax({
            url:"${pageContext.request.contextPath}/food/foodList",
            type:"get",
            data:{"foodName":foodName,"foodType":foodType,"page":page,"row":row,"colName":colName,"order":order},
            dataType:"json",
            success:function (data) {
                console.log(data);
                var html = "";
                for(var i=0;i<data.info.length;i++){
                    html += "<tr>" +
                        "<td><input  type=\"checkbox\" name=\"selectId\" value='"+data.info[i].foodId+"'></td>"+
                        "<td>"+data.info[i].foodId+"</td>" +
                        "<td>"+data.info[i].foodName+"</td>" +
                        "<td>"+data.info[i].foodType+"</td>" +
                        "<td>"+data.info[i].foodPrice+"</td>" +
                        "<td>" +
                        "<button type='button' class=\"btn btn-success btn-sm\" onclick='updata(this)'>修改</button>" +
                        "<button type='button' class=\"btn btn-success btn-sm\" onclick='delFood(this)'>删除</button>" +
                        "</td>" +
                        "</tr>";
                }
                $("#tb").html(html);
            }
        })
    }
    function allList(obj) {
        $("input[name='selectId']").prop("checked",$(obj).prop("checked"));
    }
</script>

</body>
