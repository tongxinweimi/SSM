<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2021-4-14
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加预定</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span>添加预定</span><span id="message"></span>
        </div>
        <div class="panel-body">
            <form class="form-horizontal">
                <div class="column-drag-header">
                    <div class="">
                        <div class="form-group m-b-15">
                            <label class="control-label col-xs-2" style="margin-left:20px">用户编号:</label>
                            <div class="col-xs-3">
                                <input type="text" id="userId" class="form-control" placeholder="请输入用户编号"/>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="form-group m-b-15">
                            <label class="control-label col-xs-2" style="margin-left:20px">桌位编号:</label>
                            <div class="col-xs-3">
                                <input type="text" id="deskId" class="form-control" placeholder="请输入桌位编号"/>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="form-group m-b-15">
                            <label class="control-label col-xs-2" style="margin-left:20px">时间:</label>
                            <div class="col-xs-3">
                                <select type="text"  id="reserveTypeTime" class="form-control">
                                    <option value="">-----请选择时间段-----</option>
                                    <option value="早上">-----早上---7:00-10:00------------</option>
                                    <option value="中午">-----中午---11:00-15:00-----------</option>
                                    <option value="晚上">-----晚上---18:00-23:00-----------</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="form-group m-b-15 ">
                        <label class="control-label col-xs-2" style="margin-left:20px">预定开餐时间：</label>
                        <div class="col-xs-3">
                            <input type="datetime-local" id="reserveTime" class="form-control" placeholder="请输入会员账号"/>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="form-group m-b-15">
                        <label class="control-label col-xs-2" style="margin-left:20px">预定人数：</label>
                        <div class="col-xs-3">
                            <input type="text" id="reservePeople" class="form-control" placeholder="请输入预定人数"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4" style="margin-left:18%">
                    <button  class="btn btn-primary" onclick="addDeskReserve()" type="button" ><i class="fa fa-search"></i> 确认添加</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script>
    function addDeskReserve() {
        //获取用户输入的值
        var userId = $("#userId").val();
        var deskId = $("#deskId").val();
        var reserveTypeTime = $("#reserveTypeTime").val();
        var reserveTime = $("#reserveTime").val();
        var reservePeople = $("#reservePeople").val();
        console.log(userId);
        console.log(deskId);
        console.log(reserveTypeTime);
        console.log(typeof reserveTime);
        console.log(reservePeople);
        //发送ajax请求
        $.ajax({
            //发送的目的地
            url:"${pageContext.request.contextPath}/deskReserve/ajaxAddDeskReserveByDeskReserve",
            //以什么方式发送
            type:"get",
            //有无参数
            data:{
                "userId":userId,
                "deskId":deskId,
                "reserveTypeTime":reserveTypeTime,
                "reserveTime":reserveTime,
                "reservePeople":reservePeople,
                },
            //发送方式是get的话不用加contentType，是post的话得加contentType
            // contentType: "application/x-www-form-urlencoded",
            //返回的数据类型是什么
            dataType:"json",
            //发送成功的表现是什么（回调函数实现）
            success:function (data) {
                console.log(data.message);
                //将添加完成的信息显示在敞口页面
                confirm("添加完成！")
                //添加成功的话就跳转到查询预定的页面
                window.location.href="${pageContext.request.contextPath}/deskReserve/deskReserveUI";
            }
        });
    }
</script>
</html>
