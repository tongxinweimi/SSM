<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2021-4-14
  Time: 10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>预定界面</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- 先引入jquery在引入bootstrap，因为bootstrap是基于jquery的 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
</head>
<body onload="loadData()">
<br>
<div class="layui-row">
    <br>
    <form class="layui-form layui-col-md12 x-so">
        <input type="text" id="userId" placeholder="请输入用户编号（选填）" autocomplete="off" class="layui-input">
        <input type="text" id="deskId" placeholder="请输入桌位编号（选填）" autocomplete="off" class="layui-input">
        <input type="text" id="reserveTypeTime" placeholder="请输入早晨、中午、晚上（选填）" autocomplete="off" class="layui-input">
        <input type="text" id="reserveTime" placeholder="请输入预定开餐时间（选填）" autocomplete="off" class="layui-input">
        <input type="button" class="layui-btn" value="查询" onclick="loadData()"><i class="layui-icon">&#xe615;</i></input>
    </form>
</div>
<from>
</from>
<input type="button" class="layui-btn" onclick="batchDelete()" value="批量删除">
<input type="button" class="layui-btn" onclick="skipAddReserveUI()" value="添加">
<table class="table" >
    <thead>
    <th><input type="checkbox" name="checkedId" onclick="checkedAll(this)">全选</th>
    <th>预定编号</th>
    <th>用户编号</th>
    <th>桌位编号</th>
    <th>预定时间（早、中、晚）</th>
    <th onclick="orderBy(this,'reserve_time')">预定开餐时间 <span>↑</span></th>
    <th>预订人数</th>
    </thead>
    <tbody id="table">

    </tbody>
    <%--        <span id="message" style="color:red "></span>--%>
</table>
<div>
    <input type="button" class="layui-btn" onclick="perPage()" value="上一页">
    <input type="button" class="layui-btn" onclick="nextPage()" value="下一页">
    一共有<span id="total"></span>条预定，共有<span id="totalPage"></span>页。当前第<span id="thisPage"></span>页。
</div>
</body>

<%-- 修改模态框 --%>

<div class="modal fade" id="updateWindow" style="top:15px">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--头部-->
            <div class="modal-header" style="background-color: green; height: 20px;">
            </div>
            <div class="modal-body">
                <table class="table" border="0">

                    <tr>
                        <td>预定编号：</td>
                        <td><input type="text" id="windowDeskReserveId" class="form-control" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td>用户编号</td>
                        <td><input type="text" id="windowUserId" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>桌位编号</td>
                        <td><input type="text" id="windowDeskId" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>预定时间（早、中、晚）</td>
                        <td>
                            <select type="text"  id="windowReserveTypeTime" class="form-control">
                            <option value="">-----请选择时间段-----</option>
                            <option value="早上">-----早上---7:00-10:00------------</option>
                            <option value="中午">-----中午---11:00-15:00-----------</option>
                            <option value="晚上">-----晚上---18:00-23:00-----------</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>预定开餐时间</td>
                        <td><input type="datetime-local" id="windowReserveTime" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>预订人数</td>
                        <td><input type="text" id="windowReservePeople" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="button" onclick="updateDeskReserveBywindow()" class="layui-btn">修改</button>
                            <button type="button" onclick="closeupdateDeskReservewindow()" class="layui-btn">关闭</button>
                        <td><span style="color:red" id="addMessage"></span></td>
                        <br>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>

<script>
    //第一次加载在第一页
    var page = 1;
    //每页的数据为8条
    var row = 8;
    //以时间列排序
    var colName = "reserve_time";
    //初始默认排序
    var order = "";
    //下一页
    function nextPage() {
        page++;
        if(page > totalPage){
            page = totalPage;
        }
        loadData();
    }
    //上一页
    function perPage() {
        page--;
        if(page < 1){
            page = 1;
        }
        loadData();
    }
    //加载数据
    function loadData() {
        //获取根据条件查询的信息
        var userId = $("#userId").val();
        var deskId = $("#deskId").val();
        var reserveTypeTime = $("#reserveTypeTime").val();
        var reserveTime = $("#reserveTime").val();
        console.log(userId);
        console.log(deskId);
        console.log(reserveTypeTime);
        console.log(reserveTime);
        //发送ajax请求
        $.ajax({
            //发送的目的地
            url:"${pageContext.request.contextPath}/deskReserve/ajaxSelectDeskReserve",
            //以什么方式发送
            type:"get",
            //有无参数
            data:{"userId":userId,"deskId":deskId,"reserveTypeTime":reserveTypeTime,"reserveTime":reserveTime,"page":page,"row":row,"colName":colName,"order":order},
            //发送方式是get的话不用加contentType，是post的话得加contentType
            // contentType: "application/x-www-form-urlencoded",
            //返回的数据类型是什么
            dataType:"json",
            //发送成功的表现是什么（回调函数实现）
            success:function (data) {
                console.log(data);
                //判断后端是否查询到数据
                if(typeof(data.message) == 'string'){
                    //没有查询到数据将表格数据清空
                    $("#table").html("<span style=\"color:red \">该类型数据不存在！</span>");
                }else{
                    //取出总页数
                    totalPage = data.allPage;
                    console.log(totalPage)
                    //显示总条数
                    $("#total").text(data.total);
                    //显示总页数
                    $("#totalPage").text(data.allPage);
                    //显示当前页数
                    $("#thisPage").text(data.thisPage);
                    var html = '';
                    for(var i=0;i<data.message.length;i++){
                        html += "<tr>" +
                            "<td><input  type=\"checkbox\" name=\"checkedId\" value='"+data.message[i].deskReserveId+"'></td>" +
                            "<td>" + data.message[i].deskReserveId + "</td>" +
                            "<td>" + data.message[i].userId + "</td>" +
                            "<td>" + data.message[i].deskId + "</td>" +
                            "<td>" + data.message[i].reserveTypeTime + "</td>" +
                            "<td>" + data.message[i].reserveTime + "</td>" +
                            "<td>" + data.message[i].reservePeople + "</td>" +
                            "<td><button type='button' class=\"layui-btn\" onclick='windowUpdateDeskReserve(this)'>修改</button></td>" +
                            "<td><button type='button' class=\"layui-btn\" onclick='deleteDeskReserveById(this)'>删除</button></td>" +
                            "</tr>"
                    }
                    //将数据显示在页面中
                    $("#table").html(html);
                }
            }
        });
    }
    //排序
    function orderBy(obj,name) {
        console.log(name);
        //获取span标签中的箭头
        var v = $(obj).find("span").text();
        //选择以那列排序 name就是那列
        colName = name;
        if(v == "↑"){
            $(obj).find("span").text("↓");
            order = "desc";
        }else{
            $(obj).find("span").text("↑");
            order = "asc";
        }
        //排完重新刷新
        loadData();
    }
    //全选
    function checkedAll(obj) {
        //1 获取全选的值
        var v = $(obj).prop("checked");
        console.log(v);
        //选中所有name值为 checkedId 的复选框
        $("input[name='checkedId']").prop("checked",v);
        // $("input[name='checkedId']").prop("checked",$(obj).prop("checked"));
    }
    //批量删除
    function batchDelete(obj) {
        //获取全选的值
        var arrId = '' ;
        $("input[type='checkbox']:checked").each(function () {
            arrId += $(this).val() + ",";
        })
        console.log(arrId);
        //让用户在次确认删除
        var is = confirm("确定删除吗？");
        if(is){
            //发送ajax请求
            $.ajax({
                url:"${pageContext.request.contextPath}/deskReserve/ajaxBatchDeleteDeskReserveByCheckId",
                //以什么方式发送
                type:"get",
                //是否有参数
                data: {"arrId":arrId},
                //返回来的数据是什么
                dataType:"json",
                //成功返回的表现是什么
                success:function(data){
                    console.log(data);
                    if(data.message == "批量删除成功！"){
                        //重新加载删除后的数据
                        loadData();
                    }
                }
            });
        }
    }
    //新页面添加预定信息
    function skipAddReserveUI() {
        //跳转页面
        window.location.href="${pageContext.request.contextPath}/deskReserve/skipAddReserveUI"
    }
    //通过ID来删除DeskReserve
    function deleteDeskReserveById(obj) {
        //获取当前ID
        var deskReserveId = $(obj).parent().parent().find("td").eq(1).text();
        console.log(deskReserveId);
        //发送ajax请求
        $.ajax({
            //发送的目的地
            url:"${pageContext.request.contextPath}/deskReserve/ajaxDeleteDeskReserveById",
            //以什么方式发送
            type:"get",
            //有无参数
            data:{"deskReserveId":deskReserveId},
            //发送方式是get的话不用加contentType，是post的话得加contentType
            // contentType: "application/x-www-form-urlencoded",
            //返回的数据类型是什么
            dataType:"json",
            //发送成功的表现是什么（回调函数实现）
            success:function (data) {
                console.log(data);
                if(data.message == "删除完成！"){
                    loadData();
                }
            }
        });
    }
    //打开修改页面的窗口(obj表示这个deskReserve对象)
    function windowUpdateDeskReserve(obj) {
        $("#updateWindow").modal("show");
        //每次打开将提示信息清空
        $("#message").text("");
        //根据索引将页面中的信息添加到修改窗中
        var deskReserveId = $(obj).parent().parent().find("td").eq(1).text();
        $("#windowDeskReserveId").val(deskReserveId);
        var userId = $(obj).parent().parent().find("td").eq(2).text();
        $("#windowUserId").val(userId);
        var deskId = $(obj).parent().parent().find("td").eq(3).text();
        $("#windowDeskId").val(deskId);
        var reserveTypeTime = $(obj).parent().parent().find("td").eq(4).text();
        $("#windowReserveTypeTime").val(reserveTypeTime);
        var reserveTime = $(obj).parent().parent().find("td").eq(5).text();
        $("#windowReserveTime").val(reserveTime);
        var reservePeople = $(obj).parent().parent().find("td").eq(6).text();
        $("#windowReservePeople").val(reservePeople);
    }
    //用ajax提交修改后的数据
    function updateDeskReserveBywindow() {
        //获取用户输入的值
        var deskReserveId = $("#windowDeskReserveId").val();
        var userId = $("#windowUserId").val();
        var deskId = $("#windowDeskId").val();
        var reserveTypeTime = $("#windowReserveTypeTime").val();
        var reserveTime = $("#windowReserveTime").val();
        var reservePeople = $("#windowReservePeople").val();
        console.log(deskReserveId);
        console.log(userId);
        console.log(deskId);
        console.log(reserveTypeTime);
        console.log(reserveTime);
        console.log(reservePeople);
        //发送ajax请求
        $.ajax({
            //发送的目的地
            url:"${pageContext.request.contextPath}/deskReserve/ajaxUpdateDeskReserveByDeskReserve",
            //以什么方式发送
            type:"get",
            //有无参数
            data:{
                "deskReserveId":deskReserveId,
                "userId":userId,
                "deskId":deskId,
                "reserveTypeTime":reserveTypeTime,
                "reserveTime":reserveTime,
                "reservePeople":reservePeople,
            },
            //发送方式是get的话不用加contentType，是post的话得加contentType
            // contentType: "application/x-www-form-urlencoded",
            //返回的数据类型是什么
            dataType:"json",
            //发送成功的表现是什么（回调函数实现）
            success:function (data) {
                console.log(data.message);
                //将添加完成的信息显示在敞口页面
                confirm("修改完成！")
                //添加成功的话就跳转到查询预定的页面
                window.location.href="${pageContext.request.contextPath}/deskReserve/deskReserveUI";
            }
        });
    }
    //关闭修改模态框
    function closeupdateDeskReservewindow() {
        $("#updateWindow").modal("hide");
    }
</script>
</html>
