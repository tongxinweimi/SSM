<%--
  Created by IntelliJ IDEA.
  User: Kboss
  Date: 2021/4/12
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Title</title>
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!--fonts-->
   <%-- <link href='http://fonts.useso.com/css?family=Pathway+Gothic+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!--//fonts-->--%>
    <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- js -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <!-- js -->

</head>
<body>
<!-- banner -->
<div class="banner">
    <div class="container">
        <div class="banner-info text-center">
            <a href="index.html"><img src="${pageContext.request.contextPath}/images/13.png" alt=""/></a>
            <div class="strip"></div>
            <h1>欢迎光临世际大酒店</h1>
            <p>加入我们享受美食生活</p>
        </div>
    </div>
</div>
<!-- //banner -->
<!-- banner-bottom -->
<div class="banner-bottom">
    <div class="container">
        <div class="bottom-info">
            <div class="bottom-img">
                <img src="${pageContext.request.contextPath}/images/12.png" alt=""/>
            </div>
            <p>美好的心情从食物开始</p>
            <div class="join"><a class="hvr-bounce-to-bottom button" href="${pageContext.request.contextPath}/login/loginPage">进入</a></div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //banner-bottom -->
<!-- menu -->
<div class="menu">
    <div class="container">
        <div class="menu-info">
            <h3>今日特色菜品</h3>
            <div class="border"></div>
        </div>
        <div class="menu-grids">
            <div class="menu-grid">
                <a href="#small-dialog1" class="b-link-stripe b-animate-go  thickbox play-icon popup-with-zoom-anim"><img src="${pageContext.request.contextPath}/images/1.jpg" alt=""/>
                    <div class="caption">
                        <p>梦龙雪糕</p>
                    </div>
                </a>
            </div>
            <div class="menu-grid">
                <a href="#small-dialog2" class="b-link-stripe b-animate-go  thickbox play-icon popup-with-zoom-anim"><img src="${pageContext.request.contextPath}/images/2.jpg" alt=""/>
                    <div class="caption">
                        <p>梦龙雪糕</p>
                    </div>
                </a>
            </div>
            <div class="menu-grid">
                <a href="#small-dialog3" class="b-link-stripe b-animate-go  thickbox play-icon popup-with-zoom-anim"><img src="${pageContext.request.contextPath}/images/3.jpg" alt=""/>
                    <div class="caption">
                        <p>梦龙雪糕</p>
                    </div>
                </a>
            </div>
            <div class="menu-grid">
                <a href="#small-dialog4" class="b-link-stripe b-animate-go  thickbox play-icon popup-with-zoom-anim"><img src="${pageContext.request.contextPath}/images/4.jpg" alt=""/>
                    <div class="caption">
                        <p>梦龙雪糕</p>
                    </div>
                </a>
            </div>
            <div class="menu-grid">
                <a href="#small-dialog5" class="b-link-stripe b-animate-go  thickbox play-icon popup-with-zoom-anim"><img src="${pageContext.request.contextPath}/images/5.jpg" alt=""/>
                    <div class="caption">
                        <p>梦龙雪糕</p>
                    </div>
                </a>
            </div>
            <div class="menu-grid">
                <a href="#small-dialog6" class="b-link-stripe b-animate-go  thickbox play-icon popup-with-zoom-anim"><img src="${pageContext.request.contextPath}/images/6.jpg" alt=""/>
                    <div class="caption">
                        <p>梦龙雪糕</p>
                    </div>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- pop-up-box -->
        <!-- script for pop-up-box -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/modernizr.custom.min.js"></script>
        <link href="${pageContext.request.contextPath}/css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
        <script src="${pageContext.request.contextPath}/js/jquery.magnific-popup.js" type="text/javascript"></script>
        <!-- //script for pop-up-box -->
        <div id="small-dialog1" class="mfp-hide">
            <div class="image-top">
                <img src="${pageContext.request.contextPath}/images/1.jpg" alt="" />
            </div>
        </div>
        <div id="small-dialog2" class="mfp-hide">
            <div class="image-top">
                <img src="${pageContext.request.contextPath}/images/2.jpg" alt="" />
            </div>
        </div>
        <div id="small-dialog3" class="mfp-hide">
            <div class="image-top">
                <img src="${pageContext.request.contextPath}/images/3.jpg" alt="" />
            </div>
        </div>
        <div id="small-dialog4" class="mfp-hide">
            <div class="image-top">
                <img src="${pageContext.request.contextPath}/images/4.jpg" alt="" />
            </div>
        </div>
        <div id="small-dialog5" class="mfp-hide">
            <div class="image-top">
                <img src="${pageContext.request.contextPath}/images/5.jpg" alt="" />
            </div>
        </div>
        <div id="small-dialog6" class="mfp-hide">
            <div class="image-top">
                <img src="${pageContext.request.contextPath}/images/6.jpg" alt="" />
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });

            });
        </script>
        <!-- //pop-up-box -->

    </div>
</div>
<!-- //menu -->
<!-- slider -->
<div class="slider">
    <div class="container">
        <div class="slider-info">
            <h3>餐桌礼仪</h3>
            <div class="border a"></div>
        </div>
        <!-- responsiveslides -->
        <script src="${pageContext.request.contextPath}/js/responsiveslides.min.js"></script>
        <script>
            // You can also use "$(window).load(function() {"
            $(function () {
                // Slideshow 4
                $("#slider3").responsiveSlides({
                    auto: true,
                    pager: false,
                    nav: false,
                    speed: 500,
                    namespace: "callbacks",
                    before: function () {
                        $('.events').append("<li>before event fired.</li>");
                    },
                    after: function () {
                        $('.events').append("<li>after event fired.</li>");
                    }
                });
            });
        </script>
        <!-- responsiveslides -->
        <div  id="top" class="callbacks_container">
            <ul class="rslides" id="slider3">
                <li>
                    <div class="slider-text">
                        <h4>“餐桌礼仪。顾名思义，就是指在吃饭用餐时在餐桌上的礼仪常识，餐饮礼仪问题可谓源远流长。
                            据文献记载可知，至少在周代，饮食礼仪已形成一套相当完善的制度，是孔子的称赞推崇而成为历朝历代表现大国之貌、礼仪之邦、文明之所的重要方面”</h4>
                        <p>中餐的传统</p>
                    </div>
                </li>
                <li>
                    <div class="slider-text">
                        <h4>“中国与西方在饮食文化方面有不同之处，中国人比较喜欢大伙儿到茶楼，因可品尝更多款的点心及菜肴，虽然茶楼都备有二人饭桌，但大多数是供四位或以上食客用的饭桌。
                            茶楼通常很噪吵，气氛不像西式餐厅浪漫，若二人吃饭，可考虑不选择去茶楼。”</h4>
                        <p>文化差异</p>
                    </div>
                </li>
                <li>
                    <div class="slider-text">
                        <h4>“喝汤时要用汤匙，而不是将整个碗端起来喝，用汤匙喝汤时，汤匙应该由自己这边向外舀，切忌任意搅和热汤或用嘴吹凉。
                            喝汤时避免出声是最起码的礼貌，当汤快喝完时，可将汤盘用左手拇指和食指托起，向外倾斜以便取汤。
                            喝完汤之后，汤匙应该放在汤盘或汤杯的碟子上。”</h4>
                        <p>喝汤礼仪</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- //slider -->
<!-- events -->
<div class="slider-bottom">
    <div class="container">
        <div class="events-info">
            <h3>世际酒店</h3>
            <div class="border"></div>
            <p>世际酒店坐落于成都市金牛区，这里风景秀丽，虽不在繁华地段，但优美宁静的环境一直都是酒店的特色
            我们致力于更好的服务，更诱人的美食，以及更舒适的体验。这里不仅是用餐的地方，更是您放松的乐园</p>
        </div>
        <div class="event-grids">
            <div class="col-md-6 event-grid">
                <h3><a href="single.html">美食大赛</a></h3>
                <h4>2015年6月10日</h4>
                <p>在第三届成都美食大赛中，世际酒店荣获最具创意美食奖，业内顶级厨师郭道尔亲自为我们颁奖
                我们将更加努力，致力于做出更加优秀的美食</p>
                <a href="single.html"><img src="${pageContext.request.contextPath}/images/9.jpg" alt=""/></a>
            </div>
            <div class="col-md-6 event-grid">
                <h3><a href="single.html">舒适环境</a></h3>
                <h4>2018年10月3日</h4>
                <p>适逢国庆佳节，市长亲临世际酒店视察酒店服务，监督酒店食品、环境质量，视察期间，
                市长对酒店环境赞不绝口，世际酒店将更加努力，只为提供优秀服务</p>
                <a href="single.html"><img src="${pageContext.request.contextPath}/images/10.jpg" alt=""/></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //events -->
<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="footer-header">
            <h3>营业 - 加盟</h3>
            <div class="border b"></div>
        </div>
        <div class="footer-grids">
            <div class="col-md-3 footer-grid">
                <p>工作日<span>7.00-21.00</span></p>
            </div>
            <div class="col-md-3 footer-grid">
                <p>周末<span>7.00-24.00</span></p>
            </div>
            <div class="col-md-3 footer-grid">
                <p>加盟地址<span>成都金牛区201号</span></p>
            </div>
            <div class="col-md-3 footer-grid">
                <p>加盟热线<span><a href=""></a>+8 800 555 555 55</span></p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="social-icons">
            <ul>
                <li><a class="s" href="#"></a></li>
                <li><a class="twit" href="#"></a></li>
                <li><a class="fb" href="#"></a></li>
            </ul>
        </div>
    </div>
</div>
<!-- //footer -->
</body>
</html>