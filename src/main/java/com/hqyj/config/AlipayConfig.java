package com.hqyj.config;
import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */
public class AlipayConfig {
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000117627866";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key ="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC0g3ack15Qej0toP/pibVreo/Owqle9N4VYiZ7m8xfdt+8G2QOQHA/OSJEEUPTNTGPu9AAPrxw0xkJt6tHoAArN7ioHDUqrx4hobT0g5p9DhVJd5rFSxX7QpsVonCGuw8nkRdJUn0ATjvMrt4hi+np9jriSfA2U5sgAeyxaDLGyirevj6vkxbkQJm7kMumw1umaLueQ9TbTA1EMQvnCgMhoJG6iQrObJEoomqm3TsUEKzADbKGXOOhcK8pw2zpt3j5fWx904lzjLpmWimDGcLdm0qxM7roD5nw/LXK0a6g74B18ZhWPRYF7xIIKoOL8hEZue+AHEC9KmIkf8+ScALxAgMBAAECggEAORZSTpY0f5MJAgMmq77HRyRNkn/xfHxIN88AiJru+Uno2951uCQc24HAGnKzVno+IEyvLUSp56x7AJ27Zz1skD5hAZlaHxOendg8GA/+rYnkpYVTxw1F+F5KGX9zwsgptMOn76JMseoL82FiEo3i/gDHAoWLmEK8jlTNfBWN6VvBb4OqYnzBz3ywggJRrQvwsU30aLo6xG9wZ3k3akIT6Y4/+5HBvFwKQfyWnFWCzSdPpA5q8Wwa2YVQDPzP68B4kybFKuh0oU8tE9gFXo/vsSb9LHkOBCrKXCcOtlJTXRXDx+6jOb0Ln/1eHS69JB5LOQkXe32AN2e62AUVrYjlGQKBgQDxOlMUmwhMKDiRmE3DXTLvHkTgoWp6hTNbSlkpZ8cLPBYnhhFyBuvoOLXRnY7SlhsqlJxUmIsLWvAj8n85uUe5+cYaDRdyz5pVuox3/Sx1Y1SJM7qk2O+LLCxgxLd8IuDTkal8DsWMxgwgQqhfKeAp7MliGUCmcO1U/r63bDNkbwKBgQC/kVWGiu5LA/xfNUj3DWRITSpUyoVE6Q29xdj8ROTfGV27ex8q7CPqjHur+O0v0IQWAHuuN3J8oYHIxdF9z5H0wgPymHyn+QCqvd9qWIfq0vPloz3RJYVooK4bf2HwfhfBZjDmWOdzJuyHK4wLTHYoM+1Cqs3rtWamCgNtJUp+nwKBgDlzIoYtxfQdQBusohh9AbxhARZeWvWC85AyTLnHtWe/j6cqDs2RSG2t099wHquYhC/H5NNg4Px7U+W7wwEEV0i7a6qYo5T8TE1OeFW7PfNXDLMAVtsbrzD9/DOvpeMyi0jXz/1zpyi3ofus5FyFpJVXhz6NtQ/upRcw9ErKwhKRAoGAav+FEqH02Sd/xbngaEUtlwiqtTNs3oGBcFbajIT6hPYJGZd6YCIHKZGP3g0qhcNkk1lvu24y3xPihr6e9TOD5X6cb9rl/Xgh/DwTVrXeLLXpDoFwHKplanwc4zTyAj6bjCVE+NP9ap76C8Gx9fmeIXRneA4o+BeZHvKG2WazxicCgYEA0F8E49INz8u5qTrHttY6DVrBhs5U5JWs3bqR5cuX+AEjCmhm8vWhMfAl+DXVHlBo4+O6lm8ktWgANxa0rTEsSlUz1U0kFq2hP5vT7bzdH09udumUYmiBfwlz/NbWAsgcWOSaG71rdDO59NLDPRrY/BSmbPS3jKUAYpoh97jklcA=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlg2oGNJTwfH/22o2eDl8ssV8pxSzA1vJpXcybeYoktzT2ifv/TcCi2xxgkT4zRpUiVfZKZ3qMahQPivqFcOZrvj/VXps3wM05uiJ3iC6DPYwrXdwcdZRzvKn70L9UGN4Kd1EmYfx0XFXqj6YUbOJ9i4HatKgVpAgf8D8arGgjoV+USwRaUGHdmfDpygB/FVFJou6aX2S04Ek+dN+/B+ndFt2Bp2jhqLc5qeDhUA3u9fCh7+9/f3u+H/hrmKA0TzznHr2iUiserOYrLzjK44N6qAgDuAU0QrTKKT6idm72KvJXKuTU2BM1oYJ+mHxb11KCDldTdVTdJy2KDpSLWu9AQIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:8080/SSM_war_exploded/success";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
