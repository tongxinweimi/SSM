package com.hqyj.service;

import com.hqyj.pojo.FoodOrder;
import com.hqyj.pojo.OrderNumber;

import java.util.HashMap;

/**
 * @Classname OrderNumberService
 * @Description TODO
 * @Date 2021/4/14 21:39
 * @Created by Fantasy
 */
public interface OrderNumberService {
    HashMap<String, Object> addOrderNumber(String userId,String foodIds,FoodOrder foodOrder, OrderNumber orderNumber);

    HashMap<String,Object> list(OrderNumber orderNumber,String uId);


}
