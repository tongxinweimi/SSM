package com.hqyj.service;

import com.hqyj.pojo.Employee;

import java.util.HashMap;

public interface EmployeeService {

    //修改员工部分信息
    HashMap<String,Object> updateEmp(Employee employee);

    //查询列表
    HashMap<String,Object> employeeList(Employee employee);

    //添加员工
    HashMap<String, Object> addEmp(Employee employee);

    //删除员工
    HashMap<String, Object> delEmp(Integer employeeId);
}
