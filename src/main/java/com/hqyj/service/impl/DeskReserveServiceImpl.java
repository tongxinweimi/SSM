package com.hqyj.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hqyj.dao.DeskReserveMapper;
import com.hqyj.dao.UserMapper;
import com.hqyj.pojo.Desk;
import com.hqyj.pojo.DeskReserve;
import com.hqyj.service.DeskReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class DeskReserveServiceImpl implements DeskReserveService {
    //注入Dao层对象
    @Autowired
    private DeskReserveMapper reserveMapper;

    @Autowired
    private UserMapper userMapper;

//    public static void main(String[] args) throws ParseException {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = sdf.parse("2021-4-14 14:07:34");
//        String date1 = sdf.format(date);
//        System.out.println(date);
//        System.out.println(sdf.format(date));
//    }

    //ajax查询所有预定(未完成模糊查询分页根据时间排序)（工作人员操作）
    public HashMap<String, Object> selectAllDeskReserve(DeskReserve deskReserve) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //分页
        //1、设置分页查询的页码，以及每页显示的行数
        PageHelper.startPage(deskReserve.getPage(),deskReserve.getRow());
        //2、查询
        List<DeskReserve> deskReserveList = reserveMapper.selectAllDeskReserve(deskReserve);
        //3、获取分页对象
        PageInfo<DeskReserve> pageInfo = new PageInfo<DeskReserve>(deskReserveList);
        //判断是否查询到数据
        if(deskReserveList.size() != 0){
            //4、将一个页面的所有数据装到一个集合中（一个页面一个集合）
            map.put("message",pageInfo.getList());
            //5、获取已经查询到的所有的数据总条数
            Long total = pageInfo.getTotal();
            //6、获取总页数
            int allPage = pageInfo.getPages();
            //获取上一页
            int prePage = pageInfo.getPrePage();
            //得到当前的页数码
            int thisPage = ++prePage;
            //将得到的所有信息装入集合
            map.put("total",total);
            map.put("allPage",allPage);
            map.put("thisPage",thisPage);
        }else{
            map.put("message","符合该条件的预定不存在！");
        }
        return map;
    }
    //批量删除
    public HashMap<String, Object> ajaxBatchDeleteDeskReserveByCheckId(String arrId) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        //判断是否传进来数据
        if(arrId != null){
            //将前端发送过来的数据取出来
            String arr[] = arrId.split(",");
            //将数组转换成集合
            List<String> listId = Arrays.asList(arr);
            int num = reserveMapper.batchDeleteDeskReserveByCheckId(listId);
            if (num > 0){
                map.put("message","批量删除成功！");
            }else {
                map.put("message","批量删除失败！");
            }
        }
        return map;
    }
    //新页面发送ajax请求添加预定(只完成了简单的添加没有添加业务)
    public HashMap<String, Object> ajaxAddDeskReserveByDeskReserve(DeskReserve deskReserve) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        System.err.println("前端 = " + deskReserve);
        //先查询是否有该用户
        List<Integer> numList = userMapper.selectByUserId(deskReserve.getUserId());
        System.err.println("用户id集合numList = " + numList);
        if(numList.size() != 0){
            //到这里说明有用户在添加
            int num = reserveMapper.ajaxAddDeskReserveByDeskReserve(deskReserve);
            if(num == 1){
                map.put("message","添加完成！");
            }else{
                map.put("message","添加失败！");
            }
        }else{
            map.put("message","该用户不存在！");
        }
        return map;
    }

    //ajax通过ID删除DeskReserve
    public HashMap<String, Object> ajaxDeleteDeskReserveById(DeskReserve deskReserve) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        int num = reserveMapper.deleteByPrimaryKey(deskReserve.getDeskReserveId());
        if(num != 0){
            map.put("message","删除完成！");
        }else{
            map.put("message","删除失败！");
        }
        return map;
    }

    //通过窗口发送ajax请求修改数据(未实现业务逻辑只有功能)
    public HashMap<String, Object> ajaxUpdateDeskReserveByDeskReserve(DeskReserve deskReserve) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        System.err.println("前端 = " + deskReserve);
        //先查询是否有该用户
        List<Integer> numList = userMapper.selectByUserId(deskReserve.getUserId());
        System.err.println("用户id集合numList = " + numList);
        if(numList.size() != 0){
            //到这里说明有这个用户在修改
            int num = reserveMapper.updateByPrimaryKeySelective(deskReserve);
            if(num == 1){
                map.put("message","修改成功！");
            }else{
                map.put("message","修改成功！");
            }
        }else{
            map.put("message","该用户不存在！");
        }
        return map;
    }
}
