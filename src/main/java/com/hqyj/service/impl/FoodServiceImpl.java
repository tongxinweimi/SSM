package com.hqyj.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hqyj.dao.FoodMapper;
import com.hqyj.pojo.Food;
import com.hqyj.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @Classname FoodServiceImpl
 * @Description TODO
 * @Date 2021/4/12 15:38
 * @Created by Fantasy
 */
@Service
public class FoodServiceImpl implements FoodService {
    @Autowired
    private FoodMapper foodMapper;
    public HashMap<String, Object> selectList(Food food) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        //模糊查询判断
        if(food.getFoodName()!=null && !food.getFoodName().equals("")){
            food.setFoodName("%"+food.getFoodName()+"%");
        }
        if(food.getFoodType()!=null && !food.getFoodType().equals("")){
            food.setFoodType("%"+food.getFoodType()+"%");
        }
        //1 设置分页查询的页码，每页显示的行数
        PageHelper.startPage(food.getPage(), food.getRow());
        List<Food> list =  foodMapper.selectList(food);
        //3 获取分页对象
        PageInfo<Food> pageInfo = new PageInfo<Food>(list);
        if(list.size()==0){
            map.put("info","没有查询到数据");
        }else{
            //获取当前页的集合
            map.put("info",pageInfo.getList());
            //总条数
            Long  total = pageInfo.getTotal();
            //总页数
            int  totalPage  = pageInfo.getPages();
            map.put("total",total);
            map.put("totalPage",totalPage);
        }
        return map;
    }
    public HashMap<String, Object> addFoot(Food food) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        int i = foodMapper.addFood(food);
        if (i!=0){
            map.put("info","添加成功");
        }else {
            map.put("info","添加失败");
        }
        return map;
    }
    public HashMap<String, Object> updateByPrimaryKey(Food food) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        int i = foodMapper.updateByPrimaryKey(food);
        if (i != 0){
            map.put("info","修改成功");
        }
        return map;
    }
    public HashMap<String, Object> deleteByPrimaryKey(Food food) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        int i = foodMapper.deleteByPrimaryKey(food.getFoodId());
        if (i!=0){
            map.put("info","删除成功");
        }else {
            map.put("info","删除失败");
        }
        return map;
    }
}
