package com.hqyj.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hqyj.dao.FoodMapper;
import com.hqyj.dao.FoodOrderMapper;
import com.hqyj.dao.OrderNumberMapper;
import com.hqyj.pojo.Food;
import com.hqyj.pojo.FoodOrder;
import com.hqyj.pojo.OrderNumber;
import com.hqyj.service.OrderNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @Classname OrderNumberServiceImpl
 * @Description TODO
 * @Date 2021/4/14 21:39
 * @Created by Fantasy
 */
@Service
public class OrderNumberServiceImpl implements OrderNumberService {
    @Autowired
    private OrderNumberMapper orderNumberMapper;
    @Autowired
    private FoodOrderMapper foodOrderMapper;
    @Autowired
    private FoodMapper foodMapper;

    public HashMap<String, Object> addOrderNumber(String userId,String foodIds,FoodOrder foodOrder, OrderNumber orderNumber) {
        //根据用户ID查询订单表ID
        HashMap<String, Object> map = new HashMap<String, Object>();
        FoodOrder foodOrder1 = foodOrderMapper.selectIdByUserId(Integer.valueOf(userId));
        System.out.println("_____________foodOrderId____________");
        //设置订单数量表的订单ID
        orderNumber.setFoodOrderId(foodOrder1.getFoodOrderId());
        orderNumber.setFoddNumber(1);
        //设置订单数量表的状态（默认未上菜）
        orderNumber.setType("未上菜");
        System.out.println("_____________foodId____________"+foodIds);
        String[] ids = foodIds.split(",");
        List<String> list = Arrays.asList(ids);
        System.out.println("---------------list-------------------"+list);
        for(int i = 0; i<list.size();i++){
            if(list.get(i)!=null){
                //根据订单表的ID查询foodID
                List<OrderNumber> orderList = orderNumberMapper.selectFoodIdByOrderId(orderNumber.getFoodOrderId());
                //如果订单数量表中有foodID，就将数量加1
                System.out.println("---------------------list.get(i)---------------------"+list.get(i));
                for (int o = 0;o<orderList.size();o++){
                    int id = 2;
                    System.out.println("---------------------orderList.get(i)---------------------"+orderList.get(o).getFoodId());
                    if((orderList.get(o).getFoodId()).equals(list.get(i))){
                        System.out.println("---------------------有相同FoodId---------------------");
                        orderList.get(0).setFoddNumber(orderList.get(o).getFoodId()+1);
                        orderNumberMapper.updateNumber(orderList.get(0));
                    }
                }
                orderNumber.setFoodId(Integer.valueOf(list.get(i)));
                orderNumberMapper.insert(orderNumber);
            }
        }
        map.put("info","OK");
        return map;
    }

    public HashMap<String, Object> list(OrderNumber orderNumber,String uId) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        //通过用户ID查询账单
        FoodOrder foodOrder1 = foodOrderMapper.selectFoodOrderByUserId(Integer.valueOf(uId));
        //使用账单ID查询数量
        map.put("foodOrder",foodOrder1);
        System.out.println("---------------foods-------------------"+foodOrder1);
        List<OrderNumber> orderNumbers = orderNumberMapper.selectListByOrderId(foodOrder1.getFoodOrderId());
        List<Food> foods=new ArrayList<Food>();
        for (OrderNumber orderNumber1:orderNumbers){
            foods.add(foodMapper.selectByPrimaryKey(orderNumber1.getFoodId()));
            System.out.println("---------------foods-------------------"+foods);
            map.put("food",foods);
        }
        System.out.println("---------------orderNumbers-------------------"+orderNumbers);
        map.put("info",orderNumbers);
        return map;
    }

}
