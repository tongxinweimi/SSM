package com.hqyj.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hqyj.dao.EmployeeMapper;
import com.hqyj.pojo.Employee;
import com.hqyj.service.EmployeeService;
import com.hqyj.util.MyMdFive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    //拿到dao层接口
    @Autowired
    EmployeeMapper Emapper;

    //拿到加密工具类对象
    @Autowired
    MyMdFive myMdFive;


    public HashMap<String, Object> updateEmp(Employee employee) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        //如果改密了给密码加密
        if(employee.getEmployeePassword()!=null){
            employee.setEmployeePassword( myMdFive.encrypt(employee.getEmployeePassword(),employee.getEmployeeNumber()));
        }
        int i = Emapper.updateByPrimaryKeySelective(employee);
        if(i>0){
            map.put("info","修改成功");
        }else {
            map.put("info","修改失败");
        }
        return map;
    }

    public HashMap<String, Object> employeeList(Employee employee) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        if(employee.getEmployeeName()!=null&&!"".equals(employee.getEmployeeName())){

            employee.setEmployeeName("%"+employee.getEmployeeName()+"%");
        }else {
            employee.setEmployeeName(null);
        }
        if(employee.getEmployeeAdr()!=null&&!"".equals(employee.getEmployeeAdr())){

            employee.setEmployeeAdr("%"+employee.getEmployeeAdr()+"%");
        }else {
            employee.setEmployeeAdr(null);
        }
        //分页
        // 设置分页查询的页码，每页显示的行数
        PageHelper.startPage(employee.getPage(), employee.getRow());
        //查询
        List<Employee> list = Emapper.selectByEmp(employee);
        //获取分页对象
        PageInfo<Employee> pageInfo = new PageInfo<Employee>(list);
        if(list.size()>0){
            //获取当前页的集合
            map.put("info",pageInfo.getList());
            //总条数
            Long total = pageInfo.getTotal();
            //总页数
            int totalPage = pageInfo.getPages();
            //全部返回
            map.put("total",total);
            map.put("totalPage",totalPage);
        }else {
            map.put("info","您查询的数据不存在！");
        }
        return map;
    }

    public HashMap<String, Object> addEmp(Employee employee) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        //密码加密
        if(employee.getEmployeePassword()!=null&&!"".equals(employee.getEmployeePassword())){
            employee.setEmployeePassword( myMdFive.encrypt(employee.getEmployeePassword(),employee.getEmployeeNumber()));
        }
        int i = Emapper.insert(employee);
        if(i>0){
            map.put("info","添加成功");
        }else {
            map.put("info","添加失败");
        }
        return map;
    }

    public HashMap<String, Object> delEmp(Integer employeeId) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        int i = Emapper.deleteByPrimaryKey(employeeId);
        if(i>0){
            map.put("info","删除成功！");
        }else {
            map.put("info","删除失败！");
        }
        return map;
    }
}
