package com.hqyj.service.impl;

import com.hqyj.dao.FoodOrderMapper;
import com.hqyj.pojo.Food;
import com.hqyj.pojo.FoodOrder;
import com.hqyj.service.FoodOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @Classname FoodOrderServiceImpl
 * @Description TODO
 * @Date 2021/4/14 11:16
 * @Created by Fantasy
 */
@Service
public class FoodOrderServiceImpl implements FoodOrderService {
    @Autowired
    private FoodOrderMapper foodOrderMapper;

    public HashMap<String, Object> addFoodOrder(FoodOrder foodOrder) {
        HashMap<String, Object> map = new  HashMap<String, Object>();
        int i = foodOrderMapper.insert(foodOrder);
        if (i!=0){
            map.put("info","OK");
        }
        return map;
    }


    public HashMap<String, Object> pay(Integer userId) {
        HashMap<String, Object> map = new HashMap<String, Object>();
       int a = foodOrderMapper.updataPay(userId);
        if(a!=0 ){
            map.put("info","OK");
        }
        return map;
    }
}
