package com.hqyj.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hqyj.dao.DeskMapper;
import com.hqyj.pojo.Desk;
import com.hqyj.service.DeskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class DeskServiceImpl implements DeskService {

    //注入对象
    @Autowired
    private DeskMapper deskDao;

    //ajax查询所有桌子(已完成模糊查询分页根据座位排序)
    public HashMap<String, Object> selectAllDesk(Desk desk) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        //分页
        //1、设置分页查询的页码，以及每页显示的行数
        PageHelper.startPage(desk.getPage(),desk.getRow());
        //2、查询
        List<Desk> deskList = deskDao.selectAllDesk(desk);
        //3、获取分页对象
        PageInfo<Desk> pageInfo = new PageInfo<Desk>(deskList);
        for (Desk desk1 : deskList) {
            System.err.println(desk1);
        }
        //判断是否查询到数据
        if(deskList.size() != 0){
            //4、将一个页面的所有数据装到一个集合中（一个页面一个集合）
            map.put("message",pageInfo.getList());
            //5、获取已经查询到的所有的数据总条数
            Long total = pageInfo.getTotal();
            //6、获取总页数
            int allPage = pageInfo.getPages();
            //获取上一页
            int prePage = pageInfo.getPrePage();
            //得到当前的页数码
            int thisPage = ++prePage;
            //将得到的所有信息装入集合
            map.put("total",total);
            map.put("allPage",allPage);
            map.put("thisPage",thisPage);
        }else{
            map.put("message","符合该条件的类型不存在！");
        }
        return map;
    }

    //ajax通过敞口新增桌子
    public HashMap<String, Object> ajaxAddDeskByDesk(Desk desk) {
        System.err.println("前端 = " + desk);
        HashMap<String,Object> map = new HashMap<String, Object>();
        //添加之前先查询是否重名
        Desk desk_db = deskDao.selectByPrimaryKey(desk.getDeskId());
        System.out.println(desk_db);
        if(desk_db != null){
            map.put("message","该桌子已存在哦！");
        }else{
            int num =  deskDao.insert(desk);
            if(num != 0){
                map.put("message","添加成功！");
            }else{
                map.put("message","添加失败！");
            }

        }
        return map;
    }

    //ajax通过桌子ID删除桌子
    public HashMap<String, Object> ajaxDeleteDeskById(Desk desk) {
        HashMap<String,Object> map = new HashMap<String, Object>();
        int num = deskDao.deleteByPrimaryKey(desk.getDeskId());
        if(num != 0){
            map.put("message","删除成功！");
        }else{
            map.put("message","删除失败！");
        }
        return map;
    }

    //窗口修改过后的数据通过ajax将数据持久化
    public HashMap<String, Object> ajaxUpdateDeskByDesk(Desk desk) {
        HashMap<String,Object> map = new HashMap<String, Object>();

        int n = deskDao.updateByPrimaryKeySelective(desk);
        if(n > 0){
            map.put("message","修改成功！");
        }else{
            map.put("message","修改失败！");
        }
        return map;
    }

    //批量删除
    public HashMap<String, Object> ajaxBatchDeleteDeskByCheckId(String arrId) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        //判断是否传进来数据
        if(arrId != null){
            //将前端发送过来的数据取出来
            String arr[] = arrId.split(",");
            //将数组转换成集合
            List<String> listId = Arrays.asList(arr);
            int num = deskDao.batchDeleteDeskByCheckId(listId);
//            System.out.println(num);
            if (num > 0){
                map.put("message","批量删除成功！");
            }else {
                map.put("message","批量删除失败！");
            }
        }
        return map;
    }
}
