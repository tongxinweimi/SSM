package com.hqyj.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hqyj.dao.UserMapper;
import com.hqyj.pojo.User;
import com.hqyj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    /*查询会员列表*/
    public HashMap<String, Object> memberList(User user) {
        HashMap<String, Object> map = new  HashMap<String, Object>();
        //1 设置分页查询的页码，每页显示的行数。startPage(当前页码，每页显示的行数)
        PageHelper.startPage(user.getPage(), user.getRow());
        //2 查询的列表数据
        List<User> userList = userMapper.selectUserList(user);
        //3 获取分页对象
        PageInfo<User> pageInfo = new PageInfo<User>(userList);
        //总条数
        Long  totalRows = pageInfo.getTotal();
        //总页数
        int  totalPages  = pageInfo.getPages();
        map.put("totalRows",totalRows);
        map.put("totalPages",totalPages);
        map.put("userList",userList);
        return map;
    }


    /*修改会员信息*/
    public HashMap<String, Object> updateMember(User user) {
        HashMap<String, Object> map = new  HashMap<String, Object>();
        int n  = userMapper.updateByPrimaryKeySelective(user);
        if (n>0){
            map.put("info","修改成功！");
        }else {
            map.put("info","修改失败！");
        }
        return map;
    }

    /*添加会员*/
    public HashMap<String, Object> addUser(User user) {

        HashMap<String, Object> map = new  HashMap<String, Object>();
        int n  = userMapper.insertSelective(user);
        if (n>0){
            map.put("info","添加成功！");
        }else {
            map.put("info","添加失败！");
        }
        return map;
    }

    /*通过主键删除会员*/
    public HashMap<String, Object> delUser(Integer uId) {
        HashMap<String, Object> map = new  HashMap<String, Object>();
        int n  = userMapper.deleteByPrimaryKey(uId);
        if (n>0){
            map.put("info","删除成功！");
        }else {
            map.put("info","删除失败！");
        }
        return map;
    }


}
