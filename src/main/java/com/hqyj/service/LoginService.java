package com.hqyj.service;

import com.github.qcloudsms.httpclient.HTTPException;
import com.hqyj.pojo.User;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

public interface LoginService {

    //使用shori方法验证登录
    HashMap<String,Object> loginShiro(User user);

    //注册查询是否重名
    HashMap<String,Object> repeatRegister(String userNumber);

    //注册新用户
    HashMap<String,Object> register(User user);

    //邮箱发送验证码
    HashMap<String,Object> sendEmail(String uEmail, HttpServletRequest request);

    //绑定邮箱验证
    HashMap<String, Object> registerEmail(String code, HttpServletRequest request);

    //发送手机验证码
    HashMap<String, Object> sendPhoneCode(String phone, HttpServletRequest req) throws IOException, HTTPException;

    //手机登录
    HashMap<String, Object> phoneLogin(Long phone, String phoneCode, HttpServletRequest req);
}
