package com.hqyj.service;

import com.hqyj.pojo.Desk;

import java.util.HashMap;

public interface DeskService {

    //ajax查询所有桌子(已完成模糊查询分页根据座位排序)
    HashMap<String, Object> selectAllDesk(Desk desk);

    //ajax通过敞口新增桌子
    HashMap<String, Object> ajaxAddDeskByDesk(Desk desk);

    //ajax通过桌子ID删除桌子
    HashMap<String, Object> ajaxDeleteDeskById(Desk desk);

    //窗口修改过后的数据通过ajax将数据持久化
    HashMap<String, Object> ajaxUpdateDeskByDesk(Desk desk);

    //批量删除
    HashMap<String, Object> ajaxBatchDeleteDeskByCheckId(String arrId);


}
