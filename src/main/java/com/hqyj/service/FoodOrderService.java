package com.hqyj.service;

import com.hqyj.pojo.Food;
import com.hqyj.pojo.FoodOrder;

import java.util.HashMap;

/**
 * @Classname FoodOrderService
 * @Description TODO
 * @Date 2021/4/14 11:16
 * @Created by Fantasy
 */
public interface FoodOrderService {

    HashMap<String, Object> addFoodOrder(FoodOrder foodOrder);

    HashMap<String, Object> pay(Integer userId);
}
