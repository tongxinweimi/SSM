package com.hqyj.service;

import com.hqyj.pojo.User;

import java.util.HashMap;

public interface UserService {
    
    HashMap<String, Object> memberList(User user);

    HashMap<String, Object> updateMember(User user);

    HashMap<String, Object> addUser(User user);

    HashMap<String, Object> delUser(Integer uId);

}
