package com.hqyj.service;

import com.hqyj.pojo.DeskReserve;

import java.util.HashMap;

public interface DeskReserveService {

    //ajax查询所有预定(完成模糊查询分页根据时间排序)（工作人员操作）
    HashMap<String, Object> selectAllDeskReserve(DeskReserve deskReserve);
    //批量删除
    HashMap<String, Object> ajaxBatchDeleteDeskReserveByCheckId(String arrId);
    //新页面发送ajax请求添加预定
    HashMap<String, Object> ajaxAddDeskReserveByDeskReserve(DeskReserve deskReserve);

    //ajax通过ID删除DeskReserve
    HashMap<String, Object> ajaxDeleteDeskReserveById(DeskReserve deskReserve);

    //通过窗口发送ajax请求修改数据
    HashMap<String, Object> ajaxUpdateDeskReserveByDeskReserve(DeskReserve deskReserve);
}
