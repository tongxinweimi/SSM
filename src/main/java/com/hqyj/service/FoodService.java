package com.hqyj.service;

import com.hqyj.pojo.Food;

import java.util.HashMap;

/**
 * @Classname FoodService
 * @Description TODO
 * @Date 2021/4/12 15:37
 * @Created by Fantasy
 */
public interface FoodService {

    HashMap<String, Object> selectList(Food food);

    HashMap<String, Object> addFoot(Food food);

    HashMap<String, Object> updateByPrimaryKey(Food food);

    HashMap<String, Object> deleteByPrimaryKey(Food food);
}
