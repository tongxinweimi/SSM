package com.hqyj.dao;

import com.hqyj.pojo.Employee;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Integer employeeId);

    int insert(Employee record);

    int insertSelective(Employee record);

    Employee selectByPrimaryKey(Integer employeeId);

    int updateByPrimaryKeySelective(Employee record);

    int updateByPrimaryKey(Employee record);

    //shiro登录验证
    Employee selectByEmployeeName(String userNumber);

    List<Employee> selectByEmp(Employee employee);

    Employee selectByEmployeePhone(Long phone);

    Employee selectByEmpPhone(Long phone);
}