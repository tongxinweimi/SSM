package com.hqyj.dao;

import com.hqyj.pojo.Food;
import com.hqyj.pojo.FoodOrder;
import com.hqyj.pojo.OrderNumber;

import java.util.List;

public interface FoodOrderMapper {
    int deleteByPrimaryKey(Integer foodOrderId);

    int insert(FoodOrder record);

    int insertSelective(FoodOrder record);

    FoodOrder selectByPrimaryKey(Integer foodOrderId);

    int updateByPrimaryKeySelective(FoodOrder record);

    int updateByPrimaryKey(FoodOrder record);

    FoodOrder selectIdByUserId(Integer userId);

    List<String> selectList(String uId);

    FoodOrder selectFoodOrderByUserId(Integer valueOf);

    int updataPay(Integer userId);
}