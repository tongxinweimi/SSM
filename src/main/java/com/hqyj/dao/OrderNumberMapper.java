package com.hqyj.dao;

import com.hqyj.pojo.OrderNumber;

import java.util.List;

public interface OrderNumberMapper {
    int deleteByPrimaryKey(Integer orderNumberId);

    int insert(OrderNumber record);

    int insertSelective(OrderNumber record);

    OrderNumber selectByPrimaryKey(Integer orderNumberId);

    int updateByPrimaryKeySelective(OrderNumber record);

    int updateByPrimaryKey(OrderNumber record);

    List<OrderNumber> selectFoodIdByOrderId(Integer foodOrderId);

    void updateNumber(OrderNumber order);

    List<OrderNumber> selectListByOrderId(Integer foodOrderId);
}