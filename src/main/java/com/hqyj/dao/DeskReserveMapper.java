package com.hqyj.dao;

import com.hqyj.pojo.DeskReserve;

import java.util.List;

public interface DeskReserveMapper {
    int deleteByPrimaryKey(Integer deskReserveId);

    int insert(DeskReserve record);

    int insertSelective(DeskReserve record);

    DeskReserve selectByPrimaryKey(Integer deskReserveId);

    //通过窗口发送ajax请求修改数据(未实现业务逻辑只有功能)
    int updateByPrimaryKeySelective(DeskReserve record);

    int updateByPrimaryKey(DeskReserve record);

    //ajax查询所有预定(未完成模糊查询分页根据时间排序)（工作人员操作）
    List<DeskReserve> selectAllDeskReserve(DeskReserve deskReserve);

    //批量删除
    int batchDeleteDeskReserveByCheckId(List<String> listId);

    //新页面发送ajax请求添加预定
    int ajaxAddDeskReserveByDeskReserve(DeskReserve deskReserve);

}