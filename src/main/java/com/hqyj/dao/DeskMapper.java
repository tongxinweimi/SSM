package com.hqyj.dao;

import com.hqyj.pojo.Desk;

import java.util.List;

public interface DeskMapper {
    int deleteByPrimaryKey(Integer deskId);

    int insert(Desk record);

    int insertSelective(Desk record);

    Desk selectByPrimaryKey(Integer deskId);

    int updateByPrimaryKeySelective(Desk record);

    int updateByPrimaryKey(Desk record);

    //ajax查询所有桌子(已完成模糊查询分页根据座位排序)
    List<Desk> selectAllDesk(Desk desk);

    //批量删除
    int batchDeleteDeskByCheckId(List<String> listId);
}