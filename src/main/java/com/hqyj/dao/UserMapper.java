package com.hqyj.dao;

import com.hqyj.pojo.Employee;
import com.hqyj.pojo.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer uId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer uId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //用户登录验证(shiro)
    User selectByUserName(String userNumber);

    //注册查询是否重名
    String selectByUserNumber(String userNumber);

    //查询
    List<User> selectUserList(User user);

    //预定时根据uId查询是否有该用户
    List<Integer> selectByUserId(Integer userId);


    User selectUserByPhone(Long uPhone);
}