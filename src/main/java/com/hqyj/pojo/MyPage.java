package com.hqyj.pojo;

/**
 * @Classname MyPage
 * @Description TODO
 * @Date 2021/4/2 15:36
 * @Created by Fantasy
 */
public class MyPage {
    //页码
    private int page;
    //每页显示的条数
    private int row ;

    private String colName;
    private String order;

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
