package com.hqyj.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class User extends  MyPage{
    private Integer uId;

    private String uNumber;

    private String uName;

    private String uPassword;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date uBirthday;

    private String uSex;

    private Long uPhone;

    private String uEmail;

    private String uVip;

    private Integer uIntegral;

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuNumber() {
        return uNumber;
    }

    public void setuNumber(String uNumber) {
        this.uNumber = uNumber;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public Date getuBirthday() {
        return uBirthday;
    }

    public void setuBirthday(Date uBirthday) {
        this.uBirthday = uBirthday;
    }

    public String getuSex() {
        return uSex;
    }

    public void setuSex(String uSex) {
        this.uSex = uSex;
    }

    public Long getuPhone() {
        return uPhone;
    }

    public void setuPhone(Long uPhone) {
        this.uPhone = uPhone;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getuVip() {
        return uVip;
    }

    public void setuVip(String uVip) {
        this.uVip = uVip;
    }

    public Integer getuIntegral() {
        return uIntegral;
    }

    public void setuIntegral(Integer uIntegral) {
        this.uIntegral = uIntegral;
    }

    @Override
    public String toString() {
        return "User{" +
                "uId=" + uId +
                ", uNumber='" + uNumber + '\'' +
                ", uName='" + uName + '\'' +
                ", uPassword='" + uPassword + '\'' +
                ", uBirthday=" + uBirthday +
                ", uSex='" + uSex + '\'' +
                ", uPhone=" + uPhone +
                ", uEmail='" + uEmail + '\'' +
                ", uVip='" + uVip + '\'' +
                ", uIntegral=" + uIntegral +
                '}';
    }
}