package com.hqyj.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class DeskReserve extends MyPage{
    private Integer deskReserveId;

    private Integer userId;

    private Integer deskId;

    private String reserveTypeTime;

    private String reserveTime;

    private Integer reservePeople;

    public Integer getDeskReserveId() {
        return deskReserveId;
    }

    public void setDeskReserveId(Integer deskReserveId) {
        this.deskReserveId = deskReserveId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDeskId() {
        return deskId;
    }

    public void setDeskId(Integer deskId) {
        this.deskId = deskId;
    }

    public String getReserveTypeTime() {
        return reserveTypeTime;
    }

    public void setReserveTypeTime(String reserveTypeTime) {
        this.reserveTypeTime = reserveTypeTime;
    }

    public String getReserveTime() {
        return reserveTime;
    }

    public void setReserveTime(String reserveTime) {
        this.reserveTime = reserveTime;
    }

    public Integer getReservePeople() {
        return reservePeople;
    }

    public void setReservePeople(Integer reservePeople) {
        this.reservePeople = reservePeople;
    }

    @Override
    public String toString() {
        return "DeskReserve{" +
                "deskReserveId=" + deskReserveId +
                ", userId=" + userId +
                ", deskId=" + deskId +
                ", reserveTypeTime='" + reserveTypeTime + '\'' +
                ", reserveTime='" + reserveTime + '\'' +
                ", reservePeople=" + reservePeople +
                "} " + super.toString();
    }
}