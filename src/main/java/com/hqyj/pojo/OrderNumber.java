package com.hqyj.pojo;

public class OrderNumber extends MyPage{
    private Integer orderNumberId;

    private Integer foodOrderId;

    private Integer foodId;

    private Integer foddNumber;

    private String type;

    public Integer getOrderNumberId() {
        return orderNumberId;
    }

    public void setOrderNumberId(Integer orderNumberId) {
        this.orderNumberId = orderNumberId;
    }

    public Integer getFoodOrderId() {
        return foodOrderId;
    }

    public void setFoodOrderId(Integer foodOrderId) {
        this.foodOrderId = foodOrderId;
    }

    public Integer getFoodId() {
        return foodId;
    }

    public void setFoodId(Integer foodId) {
        this.foodId = foodId;
    }

    public Integer getFoddNumber() {
        return foddNumber;
    }

    public void setFoddNumber(Integer foddNumber) {
        this.foddNumber = foddNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}