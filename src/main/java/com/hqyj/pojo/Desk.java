package com.hqyj.pojo;

public class Desk extends MyPage{

    private Integer deskId;

    private Integer deskCapacity;

    public Integer getDeskId() {
        return deskId;
    }

    public void setDeskId(Integer deskId) {
        this.deskId = deskId;
    }


    public Integer getDeskCapacity() {
        return deskCapacity;
    }

    public void setDeskCapacity(Integer deskCapacity) {
        this.deskCapacity = deskCapacity;
    }

    @Override
    public String toString() {
        return "Desk{" +
                "deskId=" + deskId +
                ", deskCapacity=" + deskCapacity +
                "} " + super.toString();
    }
}