package com.hqyj.controller;

import com.hqyj.pojo.Desk;
import com.hqyj.service.DeskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
@RequestMapping("/desk")
public class DeskController {
    //注入对象
    @Autowired
    private DeskService deskService;

    //加载并查询桌子页面
    @RequestMapping("/selectDeskUI")
    public String selectAllDesk(){
        return "desk/deskListUI";
    }

    //ajax查询所有桌子(已完成模糊查询分页根据座位排序)
    @RequestMapping("/ajaxSelectDesk")
    @ResponseBody
    public HashMap<String,Object> ajaxSelectDesk(Desk desk){
        return deskService.selectAllDesk(desk);
    }
    //ajax通过窗口新增桌子
    @RequestMapping("/ajaxAddDeskByDesk")
    @ResponseBody
    public HashMap<String,Object> ajaxAddDeskByDesk(Desk desk){
        return deskService.ajaxAddDeskByDesk(desk);
    }

    //ajax通过桌子ID删除桌子
    @RequestMapping("/ajaxDeleteDeskById")
    @ResponseBody
    public HashMap<String,Object> ajaxDeleteDeskById(Desk desk){
        return deskService.ajaxDeleteDeskById(desk);
    }
    //窗口修改过后的数据通过ajax将数据持久化
    @RequestMapping("/ajaxUpdateDeskByDesk")
    @ResponseBody
    public HashMap<String,Object> ajaxUpdateDeskByDesk(Desk desk){
        return deskService.ajaxUpdateDeskByDesk(desk);
    }
    //批量删除
    @RequestMapping("/ajaxBatchDeleteDeskByCheckId")
    @ResponseBody
    public HashMap<String,Object> ajaxBatchDeleteDeskByCheckId(String arrId){
        return deskService.ajaxBatchDeleteDeskByCheckId(arrId);
    }
}
