package com.hqyj.controller;

import com.hqyj.pojo.FoodOrder;
import com.hqyj.pojo.OrderNumber;
import com.hqyj.service.OrderNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * @Classname OrderNumberController
 * @Description TODO
 * @Date 2021/4/14 21:39
 * @Created by Fantasy
 */
@RequestMapping("/orderNumber")
@Controller
public class OrderNumberController {
    @Autowired
    private OrderNumberService orderNumberService;
    @RequestMapping("/openOrderNumber")
    @ResponseBody
    public String openOrderNumber(){
        return "orderNumber/orderNumberList";
    }
    @RequestMapping("/addOrderNumber")
    @ResponseBody
    //添加订单数量
    public HashMap<String,Object> addOrderNumber(String userId,String foodId,FoodOrder foodOrder, OrderNumber orderNumber){
        return orderNumberService.addOrderNumber(userId,foodId,foodOrder,orderNumber);
    }
    @RequestMapping("/orderNumberList")
    @ResponseBody
    public HashMap<String,Object> orderNumberList(OrderNumber orderNumber,String uId){
        return orderNumberService.list(orderNumber,uId);
    }

}
