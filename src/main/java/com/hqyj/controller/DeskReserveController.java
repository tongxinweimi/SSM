package com.hqyj.controller;

import com.hqyj.pojo.Desk;
import com.hqyj.pojo.DeskReserve;
import com.hqyj.service.DeskReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;


@Controller
@RequestMapping("/deskReserve")
public class DeskReserveController {
    //注入对象
    @Autowired
    private DeskReserveService reserveService;

    //加载并查询预定界面
    @RequestMapping("/deskReserveUI")
    public String selectAllDeskReserve(){
        return "reserve/reserveUI";
    }

    //ajax查询所有预定(完成模糊查询分页根据时间排序)（工作人员操作）
    @RequestMapping("/ajaxSelectDeskReserve")
    @ResponseBody
    public HashMap<String,Object> ajaxSelectDeskReserve(DeskReserve deskReserve){
        return reserveService.selectAllDeskReserve(deskReserve);
    }

    //批量删除
    @RequestMapping("/ajaxBatchDeleteDeskReserveByCheckId")
    @ResponseBody
    public HashMap<String,Object> ajaxBatchDeleteDeskReserveByCheckId(String arrId){
        return reserveService.ajaxBatchDeleteDeskReserveByCheckId(arrId);
    }

    //跳转到添加页面
    @RequestMapping("/skipAddReserveUI")
    public String skipAddReserveUI(){
        return "reserve/addReserveUI";
    }

    //新页面发送ajax请求添加预定
    @RequestMapping("/ajaxAddDeskReserveByDeskReserve")
    @ResponseBody
    public HashMap<String,Object> ajaxAddDeskReserveByDeskReserve(DeskReserve deskReserve) {
        return reserveService.ajaxAddDeskReserveByDeskReserve(deskReserve);
    }

    //ajax通过ID删除DeskReserve
    @RequestMapping("/ajaxDeleteDeskReserveById")
    @ResponseBody
    public HashMap<String,Object> ajaxDeleteDeskReserveById(DeskReserve deskReserve) {
        System.err.println(deskReserve.getDeskReserveId());
        return reserveService.ajaxDeleteDeskReserveById(deskReserve);
    }

    //通过窗口发送ajax请求修改数据
    @RequestMapping("/ajaxUpdateDeskReserveByDeskReserve")
    @ResponseBody
    public HashMap<String,Object> ajaxUpdateDeskReserveByDeskReserve(DeskReserve deskReserve) {
        System.err.println(deskReserve);
        return reserveService.ajaxUpdateDeskReserveByDeskReserve(deskReserve);
    }
}
