package com.hqyj.controller;

import com.hqyj.pojo.User;
import com.hqyj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
@RequestMapping("/userController")
public class UserController {
    @Autowired
    private UserService userService;

    /*会员管理页面跳转*/
    @RequestMapping("/memberManager")
    public String memberManager(){
        return "login/memberManager";
    }

    /*查询会员列表*/
    @RequestMapping("/memberList")
    @ResponseBody
    public HashMap<String,Object> memberList(User user){
        return userService.memberList(user);
    }

    /*修改会员信息*/
    @RequestMapping("/updateMember")
    @ResponseBody
    public HashMap<String,Object> updateMember(User user){
        return userService.updateMember(user);
    }

    /*添加会员*/
    @RequestMapping("/addUser")
    @ResponseBody
    public HashMap<String,Object> addUser(User user){
        return userService.addUser(user);
    }

    /*删除会员*/
    @RequestMapping("/delUser")
    @ResponseBody
    public HashMap<String,Object> delUser(Integer uId){
        return userService.delUser(uId);
    }
}
