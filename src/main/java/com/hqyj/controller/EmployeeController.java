package com.hqyj.controller;

import com.hqyj.dao.EmployeeMapper;
import com.hqyj.pojo.Employee;
import com.hqyj.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@RequestMapping("/emp")
@Controller
public class EmployeeController {

    //拿到员工dao层对象
    @Autowired
    EmployeeMapper Eapper;

    //拿到员工业务层对象
    @Autowired
    EmployeeService Eservice;


    //访问员工个人主页
    @RequestMapping("/empList")
    public String empList(Integer Id, Model model){
        Employee employee = Eapper.selectByPrimaryKey(Id);
        model.addAttribute("emp",employee);
        return "employee/employeeList";
    }

    //访问员工列表
    @RequestMapping("/employeePage")
    public String employeePage(){
        return "employee/emplist";
    }


    //修改员工信息
    @RequestMapping("/updateEmp")
    @ResponseBody
    public HashMap<String,Object> updateEmp(Employee employee){
        return Eservice.updateEmp(employee);
    }


    //查询员工列表
    @RequestMapping("/employeeList")
    @ResponseBody
    public HashMap<String,Object> employeeList(Employee employee){
        return Eservice.employeeList(employee);
    }


    //添加员工
    @RequestMapping("/addEmp")
    @ResponseBody
    public HashMap<String,Object> addEmp(Employee employee){
        return Eservice.addEmp(employee);
    }

    //删除员工
    @RequestMapping("/delEmp")
    @ResponseBody
    public HashMap<String,Object> delEmp(Integer employeeId){
        return Eservice.delEmp(employeeId);
    }
}
