package com.hqyj.controller;

import com.hqyj.pojo.Food;
import com.hqyj.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * @Classname FoodController
 * @Description TODO
 * @Date 2021/4/12 15:28
 * @Created by Fantasy
 */
@Controller
@RequestMapping("/food")
public class FoodController {
    @Autowired
    private FoodService foodService;

    @RequestMapping("/foodList")
    @ResponseBody
    public HashMap<String,Object> foodList(Food food){
        return foodService.selectList(food);
    }
    @RequestMapping("/addFood")
    @ResponseBody
    public HashMap<String,Object> addFood(Food food){
        return foodService.addFoot(food);
    }
    @RequestMapping("/updataFood")
    @ResponseBody
    public HashMap<String,Object> updataFood(Food food){
        return foodService.updateByPrimaryKey(food);
    }
    @RequestMapping("/delFood")
    @ResponseBody
    public HashMap<String,Object> delFood(Food food){
        return foodService.deleteByPrimaryKey(food);
    }

}
