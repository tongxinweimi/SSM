package com.hqyj.controller;

import com.hqyj.pojo.Food;
import com.hqyj.pojo.FoodOrder;
import com.hqyj.pojo.OrderNumber;
import com.hqyj.service.FoodOrderService;
import com.hqyj.service.OrderNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * @Classname FoodOrderController
 * @Description TODO
 * @Date 2021/4/14 11:15
 * @Created by Fantasy
 */
@Controller
@RequestMapping("/foodOrder")
public class FoodOrderController {
    @Autowired
    private FoodOrderService foodOrderService;
    @Autowired
    private OrderNumberService orderNumberService;
    @RequestMapping("/addFoodOrder")
    @ResponseBody
    //添加订单
    public HashMap<String,Object> addFoodOrder(String userId,String foodIds,FoodOrder foodOrder, OrderNumber orderNumber){
        //添加订单表
        foodOrderService.addFoodOrder(foodOrder);
        System.out.println("--------------addFoodOrder---------------");
        //把用户所选的菜添加到数据库（菜品数量表）
        return orderNumberService.addOrderNumber(userId,foodIds,foodOrder,orderNumber);
    }
}
