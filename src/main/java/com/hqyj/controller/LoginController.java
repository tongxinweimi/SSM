package com.hqyj.controller;

import com.github.qcloudsms.httpclient.HTTPException;
import com.hqyj.dao.EmployeeMapper;
import com.hqyj.dao.UserMapper;
import com.hqyj.pojo.Employee;
import com.hqyj.pojo.User;
import com.hqyj.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@RequestMapping("/login")
@Controller
public class LoginController {

    //创建登录接口，方便调用登录方法
    @Autowired
    LoginService loginService;

    //拿到客户的dao层对象
    @Autowired
    UserMapper Umapper;

    //拿到员工的导层对象
    @Autowired
    EmployeeMapper Emapper;

    //访问登录界面
    @RequestMapping("/loginPage")
    public String loginPage(){
        return "login/login";
    }

    //访问注册页面
    @RequestMapping("/registerPage")
    public String registerPage(){
        return "login/register";
    }

    //访问foodIndex页面
    @RequestMapping("/foodIndex")
    public String foodIndex(Integer Id, Model m){
        System.out.println(Id);
        User user = Umapper.selectByPrimaryKey(Id);
        m.addAttribute("user",user);
        return "food/index";
    }
    @RequestMapping("/openUI")
    public String openOrderNumber(Integer uId, Model m){
        System.out.println("-------------------login openUI------------------uId"+uId);
        User user = Umapper.selectByPrimaryKey(uId);
        System.out.println(user);
        m.addAttribute("User",user);
        return "food/number";
    }

    //发送手机登录的验证码
    @RequestMapping("/phoneLoginCode")
    @ResponseBody
    public HashMap<String, Object> phoneLoginCode(String phone, HttpServletRequest req) throws IOException, HTTPException {
        return loginService.sendPhoneCode(phone,req);
    }

    //手机登录
    @RequestMapping("/phoneLogin")
    @ResponseBody
    public HashMap<String, Object> phoneLogin(Long phone,String phoneCode, HttpServletRequest req){
        return loginService.phoneLogin(phone,phoneCode,req);
    }
    //访问indexTwo页面
    @RequestMapping("/indexTwo")
    public String indexTwo(Integer Id, Model m){
        System.out.println(Id);
        Employee employee = Emapper.selectByPrimaryKey(Id);
        m.addAttribute("emp",employee);
        return "login/indexTwo";
    }

    //访问welcom页面
    @RequestMapping("/welcome")
    public String welcome(){
        return "login/welcom";
    }


    //访问food下的list页面
    @RequestMapping("/foodListPage")
    public String foodListPage(){
        return "food/list";
    }



    //ajax请求下访问登录
    @RequestMapping("/loginShiro")
    @ResponseBody
    public HashMap<String,Object> loginShiro(User user){
        return loginService.loginShiro(user);
    }

    //ajax请求 当用户在用户账号框改变文本内容时触发查询是否重名事件
    @RequestMapping("/registerSelect")
    @ResponseBody
    public HashMap<String,Object> registerSelect(String userNumber){
        return loginService.repeatRegister(userNumber);
    }

    //ajax请求注册新用户
    @RequestMapping("/register")
    @ResponseBody
    public HashMap<String,Object> register(User user){
        return loginService.register(user);
    }

    //ajax请求邮箱发送验证码
    @RequestMapping("/sendEmail")
    @ResponseBody
    public HashMap<String,Object> sendEmail(String uEmail, HttpServletRequest request){
       return loginService.sendEmail(uEmail,request);
    }

    //ajax请求邮箱绑定验证
    @RequestMapping("/registerEmail")
    @ResponseBody
    public HashMap<String,Object> registerEmail(String code, HttpServletRequest request){
        return loginService.registerEmail(code,request);
    }
}
